# -*- coding: utf-8 -*-
import requests
import os
import datetime
import time
import codecs
import sys
import re
from threading import Thread
from threading import Lock
from bs4 import BeautifulSoup

def crawlList(url, offset = 0):
    pageUrl = url+'?offset=%s' % (offset)
    r = requests.get(pageUrl, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    urls = []
    # Acha todos os links e adiciona na resposta
    resultList = DOM.find('div', {'class': 'product-grid'})
    if resultList is not None:
        for item in resultList.findAll('div', {'class': 'product-grid-item'}):
            link = item.find('a')
            if link is not None:
                itemUrl = link.get('href')
                urls.append(urlBase+itemUrl)
        #Enquanto tiver produtos ele continua.
        if depthList == 0 or (offset + 24) < (depthList * 24):
            return urls + crawlList(url, offset + 24)
    return urls

def crawlItem(url, tags):
    r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    regex = re.compile('product-title.*')
    titulo = DOM.find('h1', {'class': regex}).string.strip().replace('"', '\\"')
    regularPrice = DOM.find('strike', {'class': 'regular-price'})
    currentPrice = DOM.find('span', {'class': 'sales-price'})
    if regularPrice is None or not regularPrice.string:
        regularPrice = currentPrice
    if regularPrice is None and currentPrice is None:
        regularPrice = 0
        currentPrice = 0
        disponivel = 0
        #raise Exception('Produto não está disponível')
    else:
        disponivel = 1
        regularPrice = regularPrice.string.strip('R$').replace('.', '').replace(',', '.').strip()
        currentPrice = currentPrice.string.strip('R$').replace('.', '').replace(',', '.').strip()
    description = DOM.find('div', {'class': 'info-description-frame-inside'})
    descriptionImgs = []
    if description is not None:
        for img in description.findAll('img'):
            descriptionImgs.append(img.get('src'))
        description = description.get_text().replace('"', '\\"')
    specs = {}
    for spec_list in DOM.findAll('tbody'):
        for spec in spec_list.findAll('tr', recursive=False):
            tds = spec.findAll('td', recursive=False)
            if tds[0].string and tds[1].string:
                tipoSpec = tds[0].string.strip()
                textoSpec = tds[1].string.strip()
                specs[tipoSpec] = textoSpec.replace('"', '\\"')
    imgList = DOM.find('div', {'class', 'image-gallery-slides'})
    imgs = []
    if imgList is not None:
        for div in imgList.findAll('div', {'class', 'image-gallery-slide'}):
            img = div.find('img')
            if img is not None:
                imgs.append(img.get('src'))

    pagamentos = []
    installment = DOM.find('div', {'class': 'installment-wrapper'})
    if installment is not None:
        p = installment.find('p')
        if p is not None and p.text != '':
            pagamentos.append({
                u'Tipo': u'Cartão de crédito',
                u'Oferta': p.text
            })
    shoptime_card = DOM.find('div', {'id': 'brandCard'})
    if shoptime_card is not None:
        try:
            div = shoptime_card.findAll('div', recursive=False)[1]
            span = div.find('span').find('span').find('span')
            if span.text != '':
                pagamentos.append({
                        u'Tipo': u'Cartão ShopTime',
                        u'Oferta': span.text
                    })
        except:
            False
    product = {
        u'Nome': titulo,
        u'Preço': regularPrice,
        u'Preço Atual': currentPrice,
        u'Características': specs,
        u'Imagens': imgs,
        u'Descrição': description,
        u'Disponível': disponivel,
        u'Url': url,
        u'Pagamentos': pagamentos,
        u'Tags': tags
    }
    return product
    # Preco = 0 e OutOfStock = 1 quando sem estoque

def writeJson(stream, item):
    stream.write('{')
    stream.write(u'"Nome":"%s",' % (item[u'Nome']))
    stream.write(u'"Url":"%s",' % (item[u'Url']))
    stream.write(u'"Preço":%s,' % (item[u'Preço']))
    stream.write(u'"Preço Atual":%s,' % (item[u'Preço Atual']))
    stream.write(u'"Disponível":%s,' % (item[u'Disponível']))
    stream.write(u'"Formas de Pagamento":[')
    for pagamento in item[u'Pagamentos']:
        stream.write('{"Tipo":"%s","Oferta":"%s"},' % (pagamento[u'Tipo'], pagamento[u'Oferta']))
    stream.write('],')
    stream.write(u'"Características":{')
    for key, value in item[u'Características'].items():
        stream.write('"%s":"%s",' % (key, value))
    stream.write('},')
    stream.write(u'"Imagens":[')
    for link in item[u'Imagens']:
        stream.write('"%s",' % (link))
    stream.write('],')
    stream.write(u'"Descrição":"%s",' % (item[u'Descrição']))
    stream.write(u'"Tags":[')
    for link in item[u'Tags']:
        stream.write('"%s",' % (link))
    stream.write(']')
    stream.write('}')

def crawlAndSave(dir, url, tags):
    try:
        product = crawlItem(url, tags)
        if product:
            productFile = codecs.open(dir+'\\%s.txt' % (re.sub('[\\\/:*?"<>|]', '', product[u'Nome'])), 'w', encoding='UTF8')
            writeJson(productFile, product)
            productFile.close()
    except Exception, e:
        lockLog.acquire()
        log = open('ShopTime\\log_%s.txt' % (datetime.datetime.now().strftime('%Y-%m-%d')), 'a')
        log.write('[Time: '+str(datetime.datetime.now())+', URL: "'+url+'"]: '+repr(e)+'\n')
        log.close
        lockLog.release()

def getCats(url):
    r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    cats = {}
    maps = DOM.find('ul', {'class': 'sitemap-list'})
    countCats = 0
    for cat in maps.findAll('li', {'class': 'sitemap-item'}, recursive=False):
        # O recursive = False garante que ele não pegue as subcategorias junto com as categorias,
        catTitle = cat.find('a').string
        catSubs = {}
        subMaps = cat.find('ul', {'class': 'sitemap-list'})
        for subCat in subMaps.findAll('li', {'class': 'sitemap-item'}, recursive=False):
            countCats += 1
            if countCats > depthCat and depthCat != 0:
                break
            subCatTitle = subCat.find('a').string
            subCatUrl = subCat.find('a').get('href')
            catSubs[subCatTitle] = urlBase+subCatUrl
        if catSubs:
            cats[catTitle] = catSubs
    return cats

def saveCats(cats, dirName):
    catListFile = codecs.open(dirName+'\\cat_list.txt', 'w', encoding='UTF8')
    catListFile.write('[')
    for catName, subCats in cats.items():
        catListFile.write('{"Nome": "%s", "SubCategorias": [' % (catName))
        for subCatName, subCatUrl in subCats.items():
            catListFile.write('{"Nome": "%s", "Url": "%s"},' % (subCatName, subCatUrl))
        catListFile.write(']},')
    catListFile.write(']')
    catListFile.close()

def crawlCat(dirName, cat, subCat, url, lockCat):
    cat = re.sub('[\\\/:*?"<>|]', '', cat)
    subCat = re.sub('[\\\/:*?"<>|]', '', subCat)
    urlItems = crawlList(url)
    threads = []
    tags = [cat, subCat]
    lockCat.acquire()
    if not os.path.exists(dirName+'\\%s' % (cat)):
        os.mkdir(dirName+'\\%s' % (cat))
    if not os.path.exists(dirName+'\\%s\\%s' % (cat, subCat)):
        os.mkdir(dirName+'\\%s\\%s' % (cat, subCat))
    if not os.path.exists(dirName+'\\%s\\%s\\Produtos' % (cat, subCat)):
        os.mkdir(dirName+'\\%s\\%s\\Produtos' % (cat, subCat))
    lockCat.release()
    for urlItem in urlItems:
        t1 = Thread(target=crawlAndSave, args=(dirName+'\\%s\\%s\\Produtos' % (cat, subCat), urlItem, tags))
        threads.append(t1)
        t1.start()
    for th in threads:
        th.join()

def main():
    cats = getCats(urlCats)

    dirName = 'ShopTime\\'
    if not os.path.exists(dirName):
        os.mkdir(dirName)

    saveCats(cats, dirName)

    threads = []
    for catNome, cat in cats.items():
        lockCat = Lock()
        for subCatNome, subCatUrl in cat.items():
            t1 = Thread(target=crawlCat, args=(dirName, catNome, subCatNome, subCatUrl, lockCat))
            threads.append(t1)
            t1.start()
    for th in threads:
        th.join()

# ----
# Página com todas as categorias do shoptime
urlCats = 'https://www.shoptime.com.br/mapa-do-site/categoria'
# Página do psvita ( 1 categoria ), para teste inicial.
urlInicial = 'https://www.shoptime.com.br/produto/133683503?pfm_carac=Acessórios%20para%20Notebook&pfm_index=3&pfm_page=category&pfm_pos=grid&pfm_type=vit_product_grid'
# URL básica da aplicação.
urlBase = 'https://www.shoptime.com.br'
# Lock para escrita do log
lockLog = Lock()
# Profundidades para testar o código sem devorar o shoptime inteiro
depthCat = 1 # 0 se ilimitado
depthList = 1 # 0 se ilimitado
# ----

if __name__ == "__main__":
    main()