# -*- coding: utf-8 -*-
import requests
import os
import datetime
import time
import codecs
import sys
import re
from threading import Thread
from threading import Lock
from bs4 import BeautifulSoup

def getCats(url):
    r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    cats = {}
    maps = DOM.find('ul', {'class': 'msDeptGrp'})
    threads = []
    for cat in maps.findAll('li', {'class': 'msDept'}, recursive=False):
        a = cat.find('a')
        urlCat = a.get('href')
        catNome = a.text
        t1 = Thread(target=getSubCats, args=(urlCat, cats, catNome))
        threads.append(t1)
        t1.start()
    for th in threads:
        th.join()
    return cats

def getSubCats(url, cats, key):
    r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    pnlFiltro = DOM.find('div', {'class':'pnlFilter'})
    if pnlFiltro is not None:
        cats[key] = {}
        for filterBox in pnlFiltro.findAll('div', {'class':'browse-box'}):
            h3 = filterBox.find('h3')
            if h3 is not None:
                a = h3.find('a')
                if a is not None:
                    url = urlBase+a.get('href')
                    subCatName = a.text
                    cats[key][subCatName] = url

def crawlCat(dirName, cat, subCat, url, lockCat):
    cat = re.sub('[\\\/:*?"<>|]', '', cat)
    subCat = re.sub('[\\\/:*?"<>|]', '', subCat)
    urlItems = crawlList(url)
    threads = []
    tags = [cat, subCat]
    lockCat.acquire()
    if not os.path.exists(dirName+'\\%s' % (cat)):
        os.mkdir(dirName+'\\%s' % (cat))
    if not os.path.exists(dirName+'\\%s\\%s' % (cat, subCat)):
        os.mkdir(dirName+'\\%s\\%s' % (cat, subCat))
    if not os.path.exists(dirName+'\\%s\\%s\\Produtos' % (cat, subCat)):
        os.mkdir(dirName+'\\%s\\%s\\Produtos' % (cat, subCat))
    lockCat.release()
    for urlItem in urlItems:
        t1 = Thread(target=crawlAndSave, args=(dirName+'\\%s\\%s\\Produtos' % (cat, subCat), urlItem, tags))
        threads.append(t1)
        t1.start()
    for th in threads:
        th.join()

def crawlList(url, pagina_atual = 1):
    pageUrl = url+'&paginaAtual=%s' % (pagina_atual)
    r = requests.get(pageUrl, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    urls = []
    # Acha todos os links e adiciona na resposta
    resultList = DOM.find('div', {'class': 'shelf-product'})
    if resultList is not None:
        for item in resultList.findAll('div', {'class': 'box-product'}):
            link = item.find('a')
            if link is not None:
                itemUrl = link.get('href')
                urls.append(itemUrl)
        #Enquanto tiver produtos ele continua.
        if depthList == 0 or pagina_atual <= depthList:
            return urls + crawlList(url, (pagina_atual + 1))
    return urls

def crawlItem(url, tags):
    ## #2
    product = {
        u'Nome': 'titulo',
        u'Preço': 'regularPrice',
        u'Preço Atual': 'currentPrice',
        u'Características': 'specs',
        u'Imagens': 'imgs',
        u'Descrição': 'description',
        u'Disponível': 'disponivel',
        u'Url': url,
        u'Pagamentos': 'pagamentos',
        u'Tags': tags
    }
    return product
    # Preco = 0 e OutOfStock = 1 quando sem estoque

def writeJson(stream, item):
    stream.write('{')
    stream.write(u'"Nome":"%s",' % (item[u'Nome']))
    stream.write(u'"Url":"%s",' % (item[u'Url']))
    stream.write(u'"Preço":%s,' % (item[u'Preço']))
    stream.write(u'"Preço Atual":%s,' % (item[u'Preço Atual']))
    stream.write(u'"Disponível":%s,' % (item[u'Disponível']))
    stream.write(u'"Formas de Pagamento":[')
    for pagamento in item[u'Pagamentos']:
        stream.write('{"Tipo":"%s","Oferta":"%s"},' % (pagamento[u'Tipo'], pagamento[u'Oferta']))
    stream.write('],')
    stream.write(u'"Características":{')
    for key, value in item[u'Características'].items():
        stream.write('"%s":"%s",' % (key, value))
    stream.write('},')
    stream.write(u'"Imagens":[')
    for link in item[u'Imagens']:
        stream.write('"%s",' % (link))
    stream.write('],')
    stream.write(u'"Descrição":"%s",' % (item[u'Descrição']))
    stream.write(u'"Tags":[')
    for link in item[u'Tags']:
        stream.write('"%s",' % (link))
    stream.write(']')
    stream.write('}')

def crawlAndSave(dir, url, tags):
    try:
        product = crawlItem(url, tags)
        if product:
            productFile = codecs.open(dir+'\\%s.txt' % (re.sub('[\\\/:*?"<>|]', '', product[u'Nome'])), 'w', encoding='UTF8')
            writeJson(productFile, product)
            productFile.close()
    except Exception, e:
        lockLog.acquire()
        log = open('CasasBahia\\log_%s.txt' % (datetime.datetime.now().strftime('%Y-%m-%d')), 'a')
        log.write('[Time: '+str(datetime.datetime.now())+', URL: "'+url+'"]: '+repr(e)+'\n')
        log.close
        lockLog.release()

def saveCats(cats, dirName):
    catListFile = codecs.open(dirName+'\\cat_list.txt', 'w', encoding='UTF8')
    catListFile.write('[')
    for catName, subCats in cats.items():
        catListFile.write('{"Nome": "%s", "SubCategorias": [' % (catName))
        for subCatName, subCatUrl in subCats.items():
            catListFile.write('{"Nome": "%s", "Url": "%s"},' % (subCatName, subCatUrl))
        catListFile.write(']},')
    catListFile.write(']')
    catListFile.close()

def main():
    cats = getCats(urlCats)

    dirName = 'CasasBahia\\'
    if not os.path.exists(dirName):
        os.mkdir(dirName)

    saveCats(cats, dirName)

    threads = []
    countCats = 0
    for catNome, cat in cats.items():
        lockCat = Lock()
        for subCatNome, subCatUrl in cat.items():
            if countCats >= depthCat:
                break
            countCats += 1
            t1 = Thread(target=crawlCat, args=(dirName, catNome, subCatNome, subCatUrl, lockCat))
            threads.append(t1)
            t1.start()
    for th in threads:
        th.join()

# ----
# Página com todas as categorias
urlCats = 'https://www.casasbahia.com.br/mapadosite/mapadosite.aspx'
# Página do psvita ( 1 categoria ), para teste inicial.
urlInicial = ''
# URL básica da aplicação.
urlBase = 'https://www.casasbahia.com.br/'
# Lock para escrita do log
lockLog = Lock()
# Profundidades para testar o código sem devorar o site inteiro
depthCat = 1 # 0 se ilimitado
depthList = 1 # 0 se ilimitado
# ----

if __name__ == "__main__":
    main()