# -*- coding: utf-8 -*-
import requests
import os
import datetime
import time
import codecs
import sys
from threading import Thread
from threading import Lock
from bs4 import BeautifulSoup

## Obs:
# Foram escaneadas 42 páginas, as últimas 2 páginas estão presentes no html de resposta mas tem a classe disabled.
# Foram encontrados 2161 produtos, o mercado livre mostra 2.724 resultados.
# Key usada: bicicleta eletrica
#
# Multithread para agilizar?
#
# Como lidar com estoque? Importante retirar do tudx quando o estoque do site vendedor acabar, principalmente para
# devedores fisicos
#

def crawllist(url, count):
    count += 1
#    if count > 1: #só para agilizar os testes
#        return []
    start = time.time()
    r = requests.get(url)
    end = time.time()
    print(url+' Pagina: '+str(count)+' - Tempo da requisicao: '+str(end-start))
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    resultList = DOM.find(id='searchResults')
    urls = []
    if resultList is not None:
        for item in resultList.findAll('li', {'class': 'results-item'}):
            itemUrl = item.find('div', {'class': 'item__info'}).find('h2', {'class': 'item__title'}).find('a').get('href')
            urls.append(itemUrl)

    paginacao = DOM.find('ul', {'class': 'andes-pagination'})
    if paginacao is not None:
        nextButton = paginacao.find('li', {'class': 'andes-pagination__button--next'})
        if nextButton is not None:
            proxUrl = nextButton.find('a').get('href')
            return urls + crawllist(proxUrl, count)
        else:
            return urls
    else:
        return urls

def crawlitem(url):
    r = requests.get(url)
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    titulo = DOM.find('h1', {'class': 'item-title__primary'}).string.strip()
    price = DOM.find('span', {'class': 'price-tag-symbol'}).get('content')
    specList = DOM.find('ul', {'class': 'specs-list'})
    specs = {}
    if specList is not None:
        for spec in specList.findAll('li', {'class': 'specs-item'}):
            tipoSpec = spec.find('strong').string.strip().strip('"')
            textoSpec = spec.find('span').string.strip()
            specs[tipoSpec] = textoSpec
    description = DOM.find('div', {'class': 'item-description__text'})

    if description is not None:
        description = description.get_text()

    imgList = DOM.find('div', {'class', 'gallery-content'})
    imgs = []
    if imgList is not None:
        for figure in imgList.findAll('figure', {'class', 'gallery-image-container'}):
            img = figure.find('a')
            if img is not None:
                imgs.append(img.get('href'))
    
    product = {
        u'Nome': titulo,
        u'Preço': price,
        u'Características': specs,
        u'Imagens': imgs,
        u'Descrição': description
    }
    return product
    ## Caso der algo errado retornar False

def writeJson(stream, item):
    stream.write('{')
    stream.write(u'"Nome":"'+item[u'Nome']+'",')
    stream.write(u'"Preço":"'+item[u'Preço']+'",')
    stream.write(u'"Características":{')
    for key, value in item[u'Características'].items():
        stream.write('"'+key+'":"'+value+'",')
    stream.write('},')
    stream.write(u'"Imagens":[')
    for link in item[u'Imagens']:
        stream.write('"'+link+'",')
    stream.write('],')
    stream.write(u'"Descrição":"'+item[u'Descrição']+'"')
    stream.write('}')

def crawlAndSave(dir, url, count):
    try:
        product = crawlitem(url)
        if product:
            productFile = codecs.open(dir+'\\'+str(count)+'.txt', 'w', encoding='UTF8')
            writeJson(productFile, product)
            productFile.close()
    except Exception, e:
        lock.acquire()
        log = open('log.txt', 'a')
        log.write('[Time: '+str(datetime.datetime.now())+']: '+str(e)+'\n')
        log.close
        lock.release()
# ----

#key = 'bicicleta eletrica'
key = 'vestuario'
#url = 'https://lista.mercadolivre.com.br/'+key.replace(' ', '-')
url = 'https://servicos.mercadolivre.com.br/vestuario/'
count = 0
urls = crawllist(url, count)

dirName = 'Mercado Livre\\'+key
if not os.path.exists(dirName):
    os.mkdir(dirName)
if not os.path.exists(dirName+'\\Produtos'):
    os.mkdir(dirName+'\\Produtos')

urlListFile = open(dirName+'\\url_list.txt', 'w')
count = 0
threads = []
lock = Lock()
for a in urls:
    #product = crawlitem(a)
    count += 1
    t1 = Thread(target=crawlAndSave, args=(dirName+'\\Produtos', a, count))
    threads.append(t1)
    t1.start()
    #print count
    #if product:
    #    productFile = codecs.open(dirName+'\\Produtos\\'+str(count)+'.txt', 'w', encoding='UTF8')
    #    writeJson(productFile, product)
    #   productFile.close()
    urlListFile.write(a+'\n')
urlListFile.close()
for th in threads:
    th.join()
print 'End'
