# -*- coding: utf-8 -*-
import requests
import os
import datetime
import time
import codecs
import sys
import re
import string
from threading import Thread
from threading import Lock
from bs4 import BeautifulSoup

# Regular
urlTeste = "https://www.submarino.com.br/produto/49037056/gift-card-digital-mario-party-para-nintendo-switch?pfm_carac=Games&pfm_index=1&pfm_page=category&pfm_pos=grid&pfm_type=vit_product_grid&sellerId"
# Promoção
#urlTeste = "https://www.submarino.com.br/produto/27701092/kit-de-acessorios-para-ps-vita-9660-preto?DCSext.recom=RR_home_page.rr2-RecentHistoricalItems&nm_origem=rec_home_page.rr2-RecentHistoricalItems&nm_ranking_rec=3&pfm_carac=Últimos%20produtos%20vistos&pfm_index=2&pfm_page=home&pfm_pos=home_page.rr2&pfm_type=vit_recommendation"
# Esgotado
#urlTeste = "https://www.submarino.com.br/produto/36848271/vingadores-os-tempo-esgotado-vol-1-panini?pfm_carac=esgotado&pfm_index=9&pfm_page=search&pfm_pos=grid&pfm_type=search_page%20&sellerId"
# Big Specs
#urlTeste = "https://www.submarino.com.br/produto/134256124/smartphone-samsung-galaxy-m30-64gb-dual-chip-android-8-1-tela-6-4-octa-core-4g-camera-13mp-5mp-5mp-azul?pfm_carac=celular&pfm_index=0&pfm_page=search&pfm_pos=grid&pfm_type=search_page%20&sellerId"
# Weird Frame
urlTeste = "https://www.submarino.com.br/produto/123021119/tablet-samsung-galaxy-tab-e-t560-8gb-wi-fi-tela-9-6-android-4-4-quad-core-preto?DCSext.recom=RR_item_page.rr2-SessionPurchaseCP&nm_origem=rec_item_page.rr2-SessionPurchaseCP&nm_ranking_rec=5"

def crawlAndSave(dir, url, count):
    #try:
    product = crawlItem(url)
    if product:
        productFile = codecs.open(str(count)+'.txt', 'w', encoding='UTF8')
        writeJson(productFile, product)
        productFile.close()
    #except Exception, e:
    #    log = open('log.txt', 'a')
    #    log.write('[Time: '+str(datetime.datetime.now())+', URL: "'+url+'"]: '+str(e)+'\n')
    #    log.close

def writeJson(stream, item):
    stream.write('{')
    stream.write(u'"Nome":"%s",' % (item[u'Nome']))
    stream.write(u'"Preço":%s,' % (item[u'Preço']))
    stream.write(u'"Preço Atual":%s,' % (item[u'Preço Atual']))
    stream.write(u'"Disponível":%s,' % (item[u'Disponível']))
    stream.write(u'"Características":{')
    for key, value in item[u'Características'].items():
        stream.write('"%s":"%s",' % (key, value))
    stream.write('},')
    stream.write(u'"Imagens":[')
    for link in item[u'Imagens']:
        stream.write('"%s",' % (link))
    stream.write('],')
    stream.write(u'"Descrição":"%s"' % (item[u'Descrição']))
    stream.write('}')

def crawlItem(url):
    r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 GTB7.1 (.NET CLR 3.5.30729)"})
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    regex = re.compile('product-title.*')
    titulo = DOM.find('h1', {'class': regex}).string.strip().replace('"', '\\"')
    regularPrice = DOM.find('strike', {'class': 'regular-price'})
    currentPrice = DOM.find('span', {'class': 'sales-price'})
    if regularPrice is None or not regularPrice.string:
        regularPrice = currentPrice
    if regularPrice is None and currentPrice is None:
        regularPrice = 0
        currentPrice = 0
        disponivel = 0
        #raise Exception('Produto não está disponível')
    else:
        disponivel = 1
        regularPrice = regularPrice.string.strip('R$').replace('.', '').replace(',', '.').strip()
        currentPrice = currentPrice.string.strip('R$').replace('.', '').replace(',', '.').strip()
    description = DOM.find('div', {'class': 'info-description-frame-inside'})
    descriptionImgs = []
    if description is not None:
        for img in description.findAll('img'):
            descriptionImgs.append(img.get('src'))
        description = description.get_text().replace('"', '\\"')
    specs = {}
    for spec_list in DOM.findAll('tbody'):
        for spec in spec_list.findAll('tr', recursive=False):
            tds = spec.findAll('td', recursive=False)
            if tds[0].string and tds[1].string:
                tipoSpec = tds[0].string.strip()
                textoSpec = tds[1].string.strip()
                specs[tipoSpec] = textoSpec.replace('"', '\\"')
    imgList = DOM.find('div', {'class', 'image-gallery-slides'})
    imgs = []
    if imgList is not None:
        for div in imgList.findAll('div', {'class', 'image-gallery-slide'}):
            img = div.find('img')
            if img is not None:
                imgs.append(img.get('src'))

    product = {
        u'Nome': titulo,
        u'Preço': regularPrice,
        u'Preço Atual': currentPrice,
        u'Características': specs,
        u'Imagens': imgs,
        u'Descrição': description,
        u'Disponível': disponivel
    }
    return product
    # Preco = 0 e OutOfStock = 1 quando sem estoque

item = crawlAndSave('', urlTeste, 1)