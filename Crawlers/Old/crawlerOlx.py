# -*- coding: utf-8 -*-
import requests
import os
import datetime
import time
import codecs
import sys
from threading import Thread
from threading import Lock
from bs4 import BeautifulSoup

def crawlList(url):
    r = requests.get(url)
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')
    urls = []
    # Acha todos os links e adiciona na resposta

    # Acha o link da próxima página e chama recursivamente a crawlList
    nextButton = None
    if nextButton is not None:
        proxUrl = nextButton.find('a').get('href')
        return urls + crawlList(proxUrl)
    else:
        return urls

def crawlItem(url):
    r = requests.get(url)
    html = r.text
    DOM = BeautifulSoup(html, 'html.parser')

def writeJson(stream, item):
    stream.write('{')

    stream.write('}')

lock = Lock()
def crawlAndSave(dir, url, count):
    try:
        product = crawlItem(url)
        if product:
            productFile = codecs.open(dir+'\\'+str(count)+'.txt', 'w', encoding='UTF8')
            writeJson(productFile, product)
            productFile.close()
    except Exception, e:
        lock.acquire()
        log = open('log.txt', 'a')
        log.write('[Time: '+str(datetime.datetime.now())+']: '+str(e)+'\n')
        log.close
        lock.release()