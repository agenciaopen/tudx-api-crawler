# -*- coding: utf-8 -*-
import scrapy
import json
import pika
from scrapy.utils.project import get_project_settings
from scrapy.http import Request
from scrapy_splash import SplashRequest
import logging

class ListasSpider(scrapy.Spider):
    name = 'listas'
    tart_urls = []
    crawl_map = {}
    produtos = {}
    shop_map = {}
    migalha = []

    custom_settings = {
        'ITEM_PIPELINES' : {
            'generico.lista_pipeline.GenericoListaPipeline': 1000
        }
    }

    page_count = 0 ##Pode ser retirado depois, está aqui só para controlar o teste.
    max_page = get_project_settings().get('MAX_PAGES') ##Pode ser retirado depois, está aqui só para controlar o teste.

    # custom_settings = {
    #     'DEFAULT_REQUEST_HEADERS' : {
    #         "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
    #         "Accept-Language": "en-US,en;q=0.9",
    #         "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.172 Safari/537.36 Vivaldi/2.5.1525.46"
    #     }
    # }

    def __init__(self, config_loja={}, urls=[], migalha=[], **kwargs):
        self.shop_map = json.loads(config_loja)
        self.crawl_map = self.shop_map['listagemEPaginacao']
        self.start_urls = json.loads(urls)
        self.migalha = json.loads(migalha)
        super().__init__(**kwargs)

    def start_requests(self):
        for url in self.start_urls:
            if("splash" in self.crawl_map):
                yield SplashRequest(url, self.parse, args=self.crawl_map["splash"])
            else:
                yield Request(url, self.parse)

    def recursive_pag(self, obj, _map):
        _temp = []
        for item in obj.css(_map['nextSelector']):
            if('filhos' in _map and _map['filhos']):
                _temp = _temp + self.recursive_pag(item, _map['filhos'][0])
            else:
                url_produto = item.css(_map['urlSelector']).extract_first()
                if url_produto:
                    url_produto = _map['dominio']+url_produto
                    _temp.append(url_produto)
        return _temp

    def recursive_next(self, obj, _map):
        _temp = False
        for item in obj.css(_map['nextSelector']):
            if('filhos' in _map and _map['filhos']):
                _temp = self.recursive_next(item, _map['filhos'][0])
            else:
                url_next = _map['dominio']+item.css(_map['urlSelector']).extract_first()
                _temp = url_next
        return _temp

    def parse(self, response):
        if self.max_page > 0 and self.page_count >= self.max_page:
            return
        self.page_count += 1
        produtos_pag = self.recursive_pag(response, self.crawl_map['listagem'])
        if not produtos_pag:
            #yield self.produtos
            return
        for produto in produtos_pag:
            yield {'url': produto}

        #self.produtos[self.page_count] = produtos_pag
        ## self.channel.basic_publish(exchange='',routing_key=self.queue_key,body=json.dumps({"config_loja": self.shop_map, "urls": produtos_pag, "migalha": self.migalha}),properties=pika.BasicProperties(content_type='text/plain',delivery_mode=2))
        if('parametroGet' in self.crawl_map['paginacao']):
            if '?' in self.start_urls[0]: #rever isso, ta meio mediocre. rever quando for realizar testes.
                next_url = self.start_urls[0]+'&'+self.crawl_map['paginacao']['parametroGet']+'='+str(self.page_count+1)
            else:
                next_url = self.start_urls[0]+'?'+self.crawl_map['paginacao']['parametroGet']+'='+str(self.page_count+1)
        else:
            next_url = self.recursive_next(response, self.crawl_map['paginacao'])

        if next_url and (self.page_count < self.max_page or self.max_page == 0):
            if("splash" in self.crawl_map):
                yield SplashRequest(next_url, self.parse, args=self.crawl_map["splash"])
            else:
                yield Request(url=next_url, callback=self.parse)
        else:
            #yield self.produtos
            return