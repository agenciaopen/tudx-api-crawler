# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import requests
import json
import logging
import scrapy
from twisted.internet import reactor, defer
# import time

class GenericoProdutoPipeline(object):
    def process_item(self, item, spider):
        # start = time.time()
        URL = 'http://tudx01/apicrawler/v1/Produto'
        r = requests.post(url = URL, json=item)
        spider.log({'Enviando...'})
        if r.status_code != 200:
            spider.log({'HTTP Code': r.status_code, 'Body': r.text})
        # end = time.time()
        # logging.warning({"Tempo de Execucao": end - start})
        return item
