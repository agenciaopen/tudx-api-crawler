# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import requests
import json
import logging
import scrapy
import pika
from scrapy.utils.project import get_project_settings
# import time

class GenericoCategoriaPipeline(object):
    
    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            shop_map=getattr(crawler.spider, 'shop_map')
        )

    def __init__(self, shop_map):
        self.shop_map = shop_map
        self.queue_key = 'categories_urls'
        self.connection = pika.BlockingConnection(pika.URLParameters('amqp://guest:guest@localhost:5672/'))
        self.channel = self.connection.channel()
        ## Colocar a conexão do pika no settings e instanciar tudo no init, maybe fechar no spider_close.
        ## Fazer o mesmo para categorias, com outra pipeline se possível.

    def process_item(self, item, spider):
        for categoria in item['categorias']:
            self.channel.basic_publish(exchange='',routing_key=self.queue_key,body=json.dumps({"config_loja": self.shop_map, "urls": [categoria['url']], "migalha": categoria['migalha']}),properties=pika.BasicProperties(content_type='text/plain',delivery_mode=2))
        return item