# -*- coding: utf-8 -*-
import scrapy
import json
import re
from scrapy.http import Request
from scrapy_splash import SplashRequest
import logging

class ProdutosSpider(scrapy.Spider):
    name = 'produtos'
    #allowed_domains = ['example.com']
    start_urls = ['']
    crawl_map = {}
    _migalha = []
    nome_loja = ""
    identificador_loja = 0

    custom_settings = {
        'ROTATING_PROXY_LIST_PATH': 'proxies.txt',
        'DOWNLOADER_MIDDLEWARES': {
            'rotating_proxies.middlewares.RotatingProxyMiddleware': 610,
            'rotating_proxies.middlewares.BanDetectionMiddleware': 620,
        }
        # ,'ITEM_PIPELINES' : {
        #     'generico.produto_pipeline.GenericoProdutoPipeline': 1000
        # }
    }

    def __init__(self, config_loja={}, urls=[], migalha=[], **kwargs):
        self.shop_map = json.loads(config_loja)
        self.crawl_map = self.shop_map['dadosProduto'][0]
        self.start_urls = json.loads(urls)
        self.nome_loja = self.shop_map['nome']
        self.identificador_loja = self.shop_map['identificador']

        if migalha:
            self._migalha = json.loads(migalha)
        super().__init__(**kwargs)

    def start_requests(self):
        for url in self.start_urls:
            if("splash" in self.crawl_map):
                yield SplashRequest(url, self.parse, args=self.crawl_map["splash"])
            else:
                yield Request(url, self.parse)

    def recursive_caracteristicas(self, obj, _map):
        _temp = []
        for item in obj.css(_map['nextSelector']):
            if('filhos' in _map and _map['filhos']):
                _temp = _temp + self.recursive_caracteristicas(item, _map['filhos'][0])
            else:
                _nome = item.css(_map['nome']).extract_first()
                _valor =  item.css(_map['valor']).extract_first()
                if _nome and _valor:
                    _carac = {
                        "nome": _nome.strip(),
                        "valor": _valor.strip()
                    }
                    _temp.append(_carac)
        return _temp

    def recursive_imagens(self, obj, _map, count = 0):
        _temp = []
        _count = 0
        for item in obj.css(_map['nextSelector']):
            if('filhos' in _map and _map['filhos']):
                _count = _count + 1
                _temp = _temp + self.recursive_imagens(item, _map['filhos'][0], _count)
            else:
                url_img = item.css(_map['urlSelector']).extract_first()
                if url_img:
                    url_img = _map['dominio']+url_img
                    if 'tratamentos' in _map:
                        for tratamento in _map['tratamentos']:
                            if tratamento['tipo'] == 'regex':
                                url_img = re.findall(tratamento['regex'], url_img)[0]
                            if tratamento['tipo'] == 'replace':
                                url_img = url_img.replace(tratamento['old'], tratamento['new'])
                    _temp.append({"ordem": count, "urlImagem": url_img})
        return _temp
    
    def recursive_campo(self, obj, _map):
        _temp = []
        for item in obj.css(_map['nextSelector']):
            if('filhos' in _map and _map['filhos']):
                 _temp.append(self.recursive_campo(item, _map['filhos'][0]))
            else:
                if('ehColecao' in _map and _map['ehColecao']):
                    valor = item.css(_map['textSelector']).extract()
                    if valor:
                        _temp = _temp + valor
                else:
                    valor = ''.join(item.css(_map['textSelector']).extract())
                    if valor:
                        _temp.append(valor)
        return _temp
    
    @staticmethod
    def regex_sub(obj, regex):
        if not obj:
            return False
        if isinstance(obj, list):
            return [ ProdutosSpider.regex_sub(each, regex) for each in obj ]
        else:
            return re.findall(regex, obj)[0]
    
    @staticmethod
    def replace_sub(obj, old, new):
        if not obj:
            return False
        if isinstance(obj, list):
            return [ ProdutosSpider.replace_sub(each, old, new) for each in obj ]
        else:
            return obj.replace(old, new)
    
    @staticmethod
    def strip_result(obj):
        if not obj:
            return False
        if isinstance(obj, list):
            _temp = []
            for each in obj:
                _aux = ProdutosSpider.strip_result(each)
                if _aux:
                    _temp.append(_aux)
            if isinstance(_temp, list) and len(_temp) == 1:
                _temp = _temp[0]
            return _temp
        else:
            return obj.strip()

    def parse(self, response):
        produto = {}
        for campo in self.crawl_map['campos']:
            produto[campo['nome']] = self.recursive_campo(response, campo)
            if 'tratamentos' in campo:
                for tratamento in campo['tratamentos']:
                    if tratamento['tipo'] == 'regex':
                        produto[campo['nome']] = ProdutosSpider.regex_sub(produto[campo['nome']], tratamento['regex'])
                    if tratamento['tipo'] == 'replace':
                        produto[campo['nome']] = ProdutosSpider.replace_sub(produto[campo['nome']], tratamento['old'], tratamento['new'])
            before = produto[campo['nome']]
            produto[campo['nome']] = ProdutosSpider.strip_result(produto[campo['nome']])

        produto['caracteristicas'] = self.recursive_caracteristicas(response, self.crawl_map['caracteristicas'])
        produto['imagens'] = self.recursive_imagens(response, self.crawl_map['imagens'])
        produto['urlAnuncio'] = response.request.url
        produto['identificadorLoja'] = self.identificador_loja

        if (not ('nomeCategoria' in produto)) or not produto['nomeCategoria']:
            produto['nomeCategoria'] = filter(None, self._migalha)
        produto['nomeCategoria'] = '|'.join(produto['nomeCategoria'])
        yield produto
