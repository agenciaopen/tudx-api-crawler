# -*- coding: utf-8 -*-
import scrapy
import json
import pika
from scrapy.utils.project import get_project_settings
from scrapy.http import Request
import logging

class CategoriasSpider(scrapy.Spider):
    name = 'categorias'
    #allowed_domains = ['example.com']
    start_urls = []
    crawl_map = {}
    shop_map = {}
    categorias = {}

    max_categories = get_project_settings().get('MAX_CATEGORIES') ##Pode ser retirado depois, está aqui só para controlar o teste.
    categorie_count = 0 ##Pode ser retirado depois, está aqui só para controlar o teste.

    custom_settings = {
        'ITEM_PIPELINES' : {
            'generico.categoria_pipeline.GenericoCategoriaPipeline': 1000
        }
    }

    # custom_settings = {
    #     'FEED_URI' : 'categories.json'
    # }

    categorie_queue = []

    def __init__(self, config_loja={}, **kwargs):
        self.shop_map = json.loads(config_loja)
        self.crawl_map = self.shop_map['categorias']
        self.start_urls = [self.crawl_map['urlBase']]
        super().__init__(**kwargs)

    def recursive_crawl(self, obj, _map, _migalha = []):
        _temp = []
        for item in obj.css(_map['nextSelector']):
            migalha = _migalha.copy()
            if self.max_categories > 0 and self.categorie_count >= self.max_categories: ##Pode ser retirado depois, está aqui só para controlar o teste.
                continue

            if('filhos' in _map and _map['filhos'] and 'urlSelector' in _map):
                url_cat = item.css(_map['urlSelector']).extract_first()
                nome_cat = item.css(_map['textSelector']).extract_first()
                migalha.append(nome_cat)
                if url_cat:
                    next_url = _map['dominio']+url_cat
                    self.categorie_queue.append({'url': next_url, 'map': _map['filhos'][0], 'migalha': migalha})
            elif('filhos' in _map and _map['filhos']):
                nome_cat = item.css(_map['textSelector']).extract_first()
                migalha.append(nome_cat)
                filhos = self.recursive_crawl(item, _map['filhos'][0], migalha)
                _temp = _temp + filhos
            else:
                self.categorie_count += 1
                nome_cat = item.css(_map['textSelector']).extract_first()
                url_cat = item.css(_map['urlSelector']).extract_first()
                if url_cat:
                    url = _map['dominio']+url_cat
                    migalha.append(nome_cat)
                    #isso aqui tem q tomar yield
                    #self.channel.basic_publish(exchange='',routing_key=self.queue_key,body=json.dumps({"config_loja": self.shop_map, "urls": [url], "migalha": migalha}),properties=pika.BasicProperties(content_type='text/plain',delivery_mode=1))
                    _temp.append({'url': url, 'migalha': migalha})
        return _temp

    def parse(self, response):
        if('_map' in response.meta):
            self.categorias = self.recursive_crawl(response, response.meta['_map'], response.meta['migalha'])
        else:
            self.categorias = self.recursive_crawl(response, self.crawl_map)

        yield {'categorias': self.categorias}

        while self.categorie_queue:
            next_pag = self.categorie_queue.pop(0)
            yield Request(url=next_pag['url'], callback=self.parse, meta={"_map": next_pag['map'], "migalha": next_pag['migalha']})