# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import requests
import json
import logging
import scrapy
import pika
from scrapy.utils.project import get_project_settings
# import time

class GenericoListaPipeline(object):
    
    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            shop_map=getattr(crawler.spider, 'shop_map'),
            migalha=getattr(crawler.spider, 'migalha')
        )

    def __init__(self, shop_map, migalha):
        self.shop_map = shop_map
        self.migalha = migalha
        self.payload_size = get_project_settings().get('MAX_PAYLOAD')
        self.queue_key = 'product_urls'
        self.connection = pika.BlockingConnection(pika.URLParameters('amqp://guest:guest@localhost:5672/'))
        self.channel = self.connection.channel()
        self.url_queue = []
        ## Colocar a conexão do pika no settings e instanciar tudo no init, maybe fechar no spider_close.
        ## Fazer o mesmo para categorias, com outra pipeline se possível.

    def process_item(self, item, spider):
        self.url_queue.append(item['url'])
        if len(self.url_queue) >= self.payload_size:
            self.empty_queue()
        return item

    def close_spider(self, spider):
        if len(self.url_queue) > 0:
            self.empty_queue()

    def empty_queue(self):
        self.channel.basic_publish(exchange='',routing_key=self.queue_key,body=json.dumps({"config_loja": self.shop_map, "urls": self.url_queue, "migalha": self.migalha}),properties=pika.BasicProperties(content_type='text/plain',delivery_mode=2))
        self.url_queue = []
