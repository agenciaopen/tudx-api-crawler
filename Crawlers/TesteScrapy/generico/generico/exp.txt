{
	"Identificador": 1,
	"UrlBase": "https://www.shoptime.com.br/mapa-do-site/categoria",
	"EhColecao": true,
    "Selector": "ul.sitemap-list > li.sitemap-item",
    "TextSelector": "a::text",
	"CategoriasFilhas":{
        "Identificador": 2,
        "UrlBase": null,
        "EhColecao": false,
        "Selector": "ul.sitemap-list > li.sitemap-item",
        "TextSelector": "a::text",
        "UrlSelector": "a[href]"
    }
}
