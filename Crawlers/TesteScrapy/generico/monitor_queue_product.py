import pika
import requests
import json

def on_message(channel, method_frame, header_frame, body):
	payload = json.loads(body.decode("utf-8"))
	PARAMS = {
				'project': 'generico',
				'spider': 'produtos',
				'urls': json.dumps(payload['urls']), 
				'config_loja': json.dumps(payload['config_loja']),
				'migalha': json.dumps(payload['migalha']),
				'setting': (payload['config_loja']['settings'])
			}
	URL = 'http://localhost:6800/schedule.json'
	r = requests.post(url = URL, data = PARAMS)
	channel.basic_ack(delivery_tag=method_frame.delivery_tag)


connection = pika.BlockingConnection(pika.URLParameters('amqp://guest:guest@localhost:5672/'))
channel = connection.channel()
channel.basic_consume('product_urls', on_message)
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()