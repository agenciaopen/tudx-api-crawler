# -*- coding: utf-8 -*-
import scrapy
import json

class ShoptimeProductSpider(scrapy.Spider):
    name = 'shoptime_product'
    #allowed_domains = ['www.shoptime.com.br']
    start_urls = []
    
    custom_settings = {
        'FEED_URI'  : 'products.json',
    }
    def __init__(self, urls=[], **kwargs):
        self.start_urls = json.loads(urls)
        super().__init__(**kwargs)
    
    def parse(self, response):
        regularPrice = response.css('.regular-price::text').extract_first()
        currentPrice = response.css('.sales-price::text').extract_first()
        if regularPrice is None or regularPrice == '':
            regularPrice = currentPrice
        if regularPrice is None and currentPrice is None:
            regularPrice = 0
            currentPrice = 0
            disponivel = 0
        else:
            disponivel = 1
            regularPrice = regularPrice.strip('R$').replace('.', '').replace(',', '.').strip()
            currentPrice = currentPrice.strip('R$').replace('.', '').replace(',', '.').strip()
        specs = {}
        for spec in response.css('tbody > tr'): #porco pra caramba esse selector, usado só para testes
            spec_nome = spec.css('td:nth-child(1) ::text').extract_first()
            spec_desc = spec.css('td:nth-child(2) ::text').extract_first()
            if spec_nome is not None and spec_desc is not None:
                specs[spec_nome] = spec_desc
        item = {
            u'Nome': response.css("[class*=product-title]::text").extract_first(),
            u'Preço regular': regularPrice,
            u'Preço atual': currentPrice,
            u'Disponibilidade': disponivel,
            u'Descrição': '\n'.join(response.css('.info-description-frame-inside ::text').extract()),
            u'Especificações': specs,
            u'Imagens': response.css('.image-gallery-slides > .image-gallery-slide > .image-gallery-image img::attr(src)').extract(),
            u'Outras Formas de Pagamento': response.css('.installment-wrapper > .payment-option::text').extract()
        }
        
        yield item
