# -*- coding: utf-8 -*-
import scrapy
import json
import io
import pika
from scrapy.utils.project import get_project_settings
from scrapy.http import Request

class ShoptimeListSpider(scrapy.Spider):
    
    def __init__(self, urls=[], **kwargs):
        self.start_urls = json.loads(urls)
        super().__init__(**kwargs)
    #    f = open("product_list.json", 'w').close()

    # def get_urls(base_url):
    #     urls = []
    #     max_cat = get_project_settings().get('MAX_CATEGORIES')
    #     with io.open('categories.json', mode='r', encoding="utf-8") as json_file:
    #         start_urls = []
    #         cat_list = json.loads(json_file.read())
    #         cat_count = 0
    #         for cat_nome, cat in cat_list.items():
    #             for sub_cat_nome, sub_cat_url in cat.items():
    #                 urls.append(base_url+sub_cat_url)
    #                 cat_count += 1
    #                 if cat_count >= max_cat:
    #                     return urls
    #     return urls

    name = 'shoptime_list'
    allowed_domains = ['www.shoptime.com.br']
    base_url = 'https://www.shoptime.com.br'
    page_count = 0
    max_page = get_project_settings().get('MAX_PAGES')

    # set queue name
    queue_key = 'product_urls'
    connection = pika.BlockingConnection(pika.URLParameters('amqp://guest:guest@localhost:5672/'))
    channel = connection.channel()

    custom_settings = {
        'FEED_URI' : 'product_list.json',
    }
    #start_urls = get_urls(base_url)
    start_urls = []

    def parse(self, response):
        self.page_count += 1
        product_list = response.css('.product-grid > .product-grid-item')
        if not product_list.extract():
            return
        page_urls = {'page': self.page_count, 'products_urls': []}
        for product in product_list:
            product_url = self.base_url+product.css('a::attr(href)').extract_first()
            page_urls['products_urls'].append(product_url)
        self.channel.basic_publish(exchange='',routing_key=self.queue_key,body=json.dumps(page_urls['products_urls']),properties=pika.BasicProperties(content_type='text/plain',delivery_mode=2))

        yield page_urls

        next_url = response.css('.pagination-product-grid > :last-child > a::attr(href)').extract_first()

        if next_url is not None and self.page_count < self.max_page:
            yield Request(url=self.base_url+next_url, callback=self.parse)
