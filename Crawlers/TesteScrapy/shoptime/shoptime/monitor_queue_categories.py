import pika
import requests
import json


def on_message(channel, method_frame, header_frame, body):
	PARAMS = {'project': 'shoptime','spider': 'shoptime_list','urls': json.dumps([body.decode("utf-8") ])}
	URL = 'http://localhost:6800/schedule.json'
	r = requests.post(url = URL, params = PARAMS)
	channel.basic_ack(delivery_tag=method_frame.delivery_tag)


connection = pika.BlockingConnection(pika.URLParameters('amqp://guest:guest@localhost:5672/'))
channel = connection.channel()
channel.basic_consume('categories_urls', on_message)
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()