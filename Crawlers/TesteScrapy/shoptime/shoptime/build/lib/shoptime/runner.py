import scrapy
from scrapy.crawler import CrawlerProcess
from spiders.shoptime_product import ShoptimeProductSpider
from spiders.shoptime_list import ShoptimeListSpider

process = CrawlerProcess()
process.crawl(ShoptimeListSpider)
process.crawl(ShoptimeProductSpider)
process.start()