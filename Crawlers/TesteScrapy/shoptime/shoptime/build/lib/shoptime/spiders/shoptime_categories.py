# -*- coding: utf-8 -*-
import scrapy
import pika
from scrapy.utils.project import get_project_settings

class ShoptimeCategoriesSpider(scrapy.Spider):
    name = 'shoptime_categories'
    allowed_domains = ['www.shoptime.com.br']
    base_url = 'https://www.shoptime.com.br'
    start_urls = ['https://www.shoptime.com.br/mapa-do-site/categoria']
    max_categories = get_project_settings().get('MAX_CATEGORIES')
    categorie_count = 0

    custom_settings = {
        'FEED_URI' : 'categories.json'
    }

    # set queue name
    queue_key = 'categories_urls'
    connection = pika.BlockingConnection(pika.URLParameters('amqp://guest:guest@localhost:5672/'))
    channel = connection.channel()
    
    def parse(self, response):
        cat_list = response.css('.sitemap-block > .sitemap-list > .sitemap-item')
        result = {}
        f = open("categories.json", 'w').close()
        for cat in cat_list:
            cat_name = cat.css('a::text').extract_first()
            result[cat_name] = {}
            for sub_cat in cat.css('.sitemap-list > .sitemap-item'):
                sub_cat_name = sub_cat.css('a::text').extract_first()
                sub_cat_url = sub_cat.css('a::attr(href)').extract_first()
                result[cat_name][sub_cat_name] = sub_cat_url
                self.channel.basic_publish(exchange='',routing_key=self.queue_key,body=self.base_url+sub_cat_url,properties=pika.BasicProperties(content_type='text/plain',delivery_mode=1))
                self.categorie_count += 1
                if self.categorie_count >= self.max_categories:
                    yield result
                    return
        
        yield result