# -*- coding: utf-8 -*-
import scrapy


class SubmarinoBotSpider(scrapy.Spider):
    name = 'submarino_bot'
    allowed_domains = ['www.submarino.com.br']
    start_urls = ['https://www.submarino.com.br/mapa-do-site/categoria']

    def parse(self, response):
        cat_list = response.css('.sitemap-list').extract_first()
        result = {}
        print('teste')
        print(cat_list)
        for cat in cat_list.css('.sitemap-item'):
            print(2)
            cat_name = cat.css('a::text').extract()
            result[cat_name] = {}
            for sub_cat in cat.css('.sitemap-list').extract_first():
                sub_cat_name = sub_cat.css('a::text').extract()
                sub_cat_url = sub_cat.css('a::attr(href)').extract()
                result[cat_name][sub_cat_name] = sub_cat_url
        
        yield result
