# -*- coding: utf-8 -*-
import scrapy


class SubmarinoCategoriesBotSpider(scrapy.Spider):
    name = 'submarino_categories_bot'
    allowed_domains = ['www.submarino.com.br']
    start_urls = ['https://www.submarino.com.br/mapa-do-site/categoria']

    def parse(self, response):
        pass
