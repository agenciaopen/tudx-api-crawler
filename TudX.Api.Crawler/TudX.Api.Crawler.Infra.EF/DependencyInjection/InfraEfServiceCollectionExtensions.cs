﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Adapter.EntityFramework;
using TudX.Adapter.EntityFramework.Context;
using TudX.Api.Crawler.Infra.EF.Context;
using TudX.Domain.Base.Repository;
using TudX.Repository.Base;
using TudX.Repository.Base.Provider;

namespace TudX.Api.Crawler.Infra.EF.DependencyInjection
{
    public static class InfraEfServiceCollectionExtensions
    {
        public static IServiceCollection AddEFAdapter(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IBaseRelationalRepository<>), typeof(BaseRelationalRepository<>));
            services.AddScoped(typeof(IBaseRepositoryRelationalProvider<>), typeof(EntityFrameworkProvider<>));
            services.AddScoped(typeof(AppDataContext), typeof(ApiCrawlerDbContext));
            services.AddDbContext<ApiCrawlerDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("ApiCrawlerConnectionString")));

            return services;
        }
    }
}
