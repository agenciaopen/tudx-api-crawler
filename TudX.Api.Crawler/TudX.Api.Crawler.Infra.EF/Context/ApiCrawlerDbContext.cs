﻿using Microsoft.EntityFrameworkCore;
using TudX.Adapter.EntityFramework.Context;
using TudX.Api.Crawler.Domain.Model;
using TudX.Api.Crawler.Infra.EF.Mappings;

namespace TudX.Api.Crawler.Infra.EF.Context
{
    public class ApiCrawlerDbContext : AppDataContext
    {
        public ApiCrawlerDbContext(DbContextOptions<ApiCrawlerDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ForSqlServerUseIdentityColumns();            

            CaracteristicaCrawlerMap.Mapper(modelBuilder);
            ConfiguracaoCaracteristicaMap.Mapper(modelBuilder);
            ConfiguracaoCategoriaMap.Mapper(modelBuilder);
            ConfiguracaoImagemMap.Mapper(modelBuilder);
            ConfiguracaoListagemMap.Mapper(modelBuilder);            
            ConfiguracaoPaginacaoMap.Mapper(modelBuilder);
            ConfiguracaoProdutoMap.Mapper(modelBuilder);
            ConfiguracaoDadosProdutoMap.Mapper(modelBuilder);
            ConfiguracaoSplashMap.Mapper(modelBuilder);
            ConfiguracaoGenericaCrawlerMap.Mapper(modelBuilder);
            ConfiguracaoLojaCrawlerMap.Mapper(modelBuilder);
            ConfiguracaoAgendamentoMap.Mapper(modelBuilder);
            ImagemCrawlerMap.Mapper(modelBuilder);
            LojaCrawlerMap.Mapper(modelBuilder);
            ProdutoCrawlerMap.Mapper(modelBuilder);
            TratamentoImagemMap.Mapper(modelBuilder);
            TratamentoProdutoMap.Mapper(modelBuilder);
            LogCrawlerMap.Mapper(modelBuilder);
            EventoEnviadoMap.Mapper(modelBuilder);
            EventoRecebidoMap.Mapper(modelBuilder);
            LogErroCrawlerMap.Mapper(modelBuilder);
        }

        public DbSet<LojaCrawler> LojaCrawler { get; set; }
        public DbSet<CaracteristicaCrawler> CaracteristicaCrawler { get; set; }
        public DbSet<ConfiguracaoCaracteristica> ConfiguracaoCaracteristica { get; set; }
        public DbSet<ConfiguracaoCategoria> ConfiguracaoCategoria { get; set; }
        public DbSet<ConfiguracaoImagem> ConfiguracaoImagem { get; set; }
        public DbSet<ConfiguracaoListagem> ConfiguracaoListagem { get; set; }        
        public DbSet<ConfiguracaoPaginacao> ConfiguracaoPaginacao { get; set; }
        public DbSet<ConfiguracaoProduto> ConfiguracaoProduto { get; set; }
        public DbSet<ImagemCrawler> ImagemCrawler { get; set; }
        public DbSet<ProdutoCrawler> ProdutoCrawler { get; set; }
        public DbSet<TratamentoImagem> TratamentoImagem { get; set; }
        public DbSet<TratamentoProduto> TratamentoProduto { get; set; }
        
    }
}
