﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.IntegrationEvents.Application.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class EventoRecebidoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EventoRecebido>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.IdentificadorEvento).IsRequired();
                ent.Property(a => a.Mensagem).IsRequired();
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.DataRecebimento).IsRequired();
            });
        }
    }
}
