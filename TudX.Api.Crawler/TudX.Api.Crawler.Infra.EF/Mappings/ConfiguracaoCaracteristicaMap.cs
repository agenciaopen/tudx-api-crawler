﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoCaracteristicaMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoCaracteristica>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.NextSelector).IsRequired();
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.Valor).IsRequired();
                ent.Property(a => a.IdentificadorConfiguracaoCaracteristicaSuperior);

                ent.HasOne(x => x.ConfiguracaoDadosProduto)
                .WithMany()
                .HasForeignKey("IdentificadorConfiguracaoDadosProduto");

                ent.HasMany(e => e.Filhos)
                   .WithOne(e => e.ConfiguracaoCaracteristicaSuperior)
                   .HasForeignKey(e => e.IdentificadorConfiguracaoCaracteristicaSuperior);
            });
        }
    }
}
