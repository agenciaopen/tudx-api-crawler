﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class CaracteristicaCrawlerMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CaracteristicaCrawler>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.Valor).IsRequired();

                ent.HasOne(x => x.Produto)
                .WithMany()
                .HasForeignKey("IdentificadorProduto");
            });
        }
    }
}
