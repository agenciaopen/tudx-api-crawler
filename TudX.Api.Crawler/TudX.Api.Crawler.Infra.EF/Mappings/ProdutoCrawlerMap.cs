﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ProdutoCrawlerMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProdutoCrawler>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.NomeCategoria).IsRequired();
                ent.Property(a => a.CodigoProduto);
                ent.Property(a => a.Descricao).IsRequired();
                ent.Property(a => a.UrlAnuncio).IsRequired();
                ent.Property(a => a.Valor).IsRequired();
                ent.Property(a => a.FormaPagamento).IsRequired();
                ent.Property(a => a.IdentificadorLoja).IsRequired();

                //ent.HasOne(x => x.Loja)
                //.WithMany()                
                //.HasForeignKey("IdentificadorLoja");

                ent.HasMany(x => x.Caracteristicas)                    
                    .WithOne(x => x.Produto);

                ent.HasMany(x => x.Imagens)
                    .WithOne(x => x.Produto);
            });
        }
    }
}
