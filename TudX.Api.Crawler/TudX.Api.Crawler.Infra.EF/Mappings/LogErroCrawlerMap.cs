﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class LogErroCrawlerMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LogErroCrawler>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.IdentificadorLoja).IsRequired();
                ent.Property(a => a.Spider).IsRequired();
                ent.Property(a => a.Message).IsRequired();
                ent.Property(a => a.HttpError);
                ent.Property(a => a.StackTrace);
            });
        }
    }
}
