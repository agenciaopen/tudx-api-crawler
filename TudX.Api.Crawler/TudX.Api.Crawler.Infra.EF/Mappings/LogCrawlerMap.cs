﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class LogCrawlerMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LogCrawler>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.IdentificadorLoja).IsRequired();
                ent.Property(a => a.DataInicio).IsRequired();
                ent.Property(a => a.MensagemStatus);
                ent.Property(a => a.TipoLog);
            });
        }
    }
}
