﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoProdutoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoProduto>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.NomeCampo).IsRequired();                
                ent.Property(a => a.NextSelector);
                ent.Property(a => a.TextSelector);
                ent.Property(a => a.EhColecao).IsRequired();
                ent.Property(a => a.IdentificadorConfiguracaoProdutoSuperior);

                ent.HasOne(x => x.ConfiguracaoDadosProduto)
                .WithMany()
                .HasForeignKey("IdentificadorConfiguracaoDadosProduto");

                ent.HasMany(e => e.Filhos)
                   .WithOne(e => e.ConfiguracaoProdutoSuperior)
                   .HasForeignKey(e => e.IdentificadorConfiguracaoProdutoSuperior);

                ent.HasMany(e => e.Tratamentos)
                   .WithOne(e => e.ConfiguracaoProduto);
            });
        }
    }
}
