﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoCategoriaMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoCategoria>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.UrlBase);
                ent.Property(a => a.UrlSelector);
                ent.Property(a => a.TextSelector);
                ent.Property(a => a.NextSelector);
                ent.Property(a => a.Dominio);
                ent.Property(a => a.IdentificadorConfiguracaoCategoriaSuperior);                

                ent.HasOne(x => x.Loja)
                .WithMany()
                .HasForeignKey("IdentificadorLoja");

                ent.HasMany(e => e.Filhos)
                   .WithOne(e => e.ConfiguracaoCategoriaSuperior)
                   .HasForeignKey(e => e.IdentificadorConfiguracaoCategoriaSuperior);
            });
        }
    }
}
