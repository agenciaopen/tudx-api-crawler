﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoAgendamentoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoAgendamento>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.IdentificadorLoja);
                ent.Property(a => a.TipoFrequencia);
                ent.Property(a => a.ValorFrequencia);
                ent.Property(a => a.DataInicio);
                ent.Property(a => a.DataFim);
            });
        }
    }
}
