﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class TratamentoProdutoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TratamentoProduto>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Tipo).IsRequired();
                ent.Property(a => a.ValorProcurado);
                ent.Property(a => a.ValorSubstituto);
                ent.Property(a => a.Regex);

                ent.HasOne(x => x.ConfiguracaoProduto)
                .WithMany(t => t.Tratamentos)
                .HasForeignKey("IdentificadorConfiguracaoProduto");
            });
        }
    }
}
