﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoLojaCrawlerMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoLojaCrawler>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Settings);
                ent.Property(a => a.IdentificadorConfiguracaoGenericaCrawler);

                ent.HasOne(x => x.Loja)
                .WithMany()
                .HasForeignKey("IdentificadorLoja");
            });
        }
    }
}
