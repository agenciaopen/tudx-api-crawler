﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoListagemMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoListagem>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.NextSelector);
                ent.Property(a => a.UrlSelector);
                ent.Property(a => a.Dominio);
                ent.Property(a => a.IdentificadorConfiguracaoListagemSuperior);

                ent.HasOne(x => x.Loja)
                .WithMany()
                .HasForeignKey("IdentificadorLoja");

                ent.HasMany(e => e.Filhos)
                   .WithOne(e => e.ConfiguracaoListagemSuperior)
                   .HasForeignKey(e => e.IdentificadorConfiguracaoListagemSuperior);
            });
        }
    }
}
