﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class TratamentoImagemMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TratamentoImagem>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Tipo).IsRequired();
                ent.Property(a => a.ValorProcurado);
                ent.Property(a => a.ValorSubstituto);                

                ent.HasOne(x => x.ConfiguracaoImagem)
                .WithMany(t => t.Tratamentos)
                .HasForeignKey("IdentificadorConfiguracaoImagem");
            });
        }
    }
}
