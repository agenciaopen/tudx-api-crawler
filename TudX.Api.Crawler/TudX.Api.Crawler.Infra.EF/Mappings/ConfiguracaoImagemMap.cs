﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoImagemMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoImagem>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.NextSelector);
                ent.Property(a => a.UrlSelector);
                ent.Property(a => a.Dominio);
                ent.Property(a => a.IdentificadorConfiguracaoImagemSuperior);

                ent.HasOne(x => x.ConfiguracaoDadosProduto)
                .WithMany()
                .HasForeignKey("IdentificadorConfiguracaoDadosProduto");

                ent.HasMany(e => e.Filhos)
                   .WithOne(e => e.ConfiguracaoImagemSuperior)
                   .HasForeignKey(e => e.IdentificadorConfiguracaoImagemSuperior);

                ent.HasMany(e => e.Tratamentos)
                   .WithOne(e => e.ConfiguracaoImagem);
            });
        }
    }
}
