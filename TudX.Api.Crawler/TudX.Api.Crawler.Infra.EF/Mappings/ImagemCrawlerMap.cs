﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ImagemCrawlerMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ImagemCrawler>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.UrlImagem).IsRequired();                

                ent.HasOne(x => x.Produto)
                .WithMany()
                .HasForeignKey("IdentificadorProduto");
            });
        }
    }
}
