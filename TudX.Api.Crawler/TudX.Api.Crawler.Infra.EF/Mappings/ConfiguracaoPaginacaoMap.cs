﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoPaginacaoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoPaginacao>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.NextSelector);
                ent.Property(a => a.UrlSelector);
                ent.Property(a => a.Dominio);
                ent.Property(a => a.ParametroGet);

                ent.HasOne(x => x.Loja)
                .WithMany()
                .HasForeignKey("IdentificadorLoja");
            });
        }
    }
}
