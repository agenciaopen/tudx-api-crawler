﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;

namespace TudX.Api.Crawler.Infra.EF.Mappings
{
    public static class ConfiguracaoDadosProdutoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracaoDadosProduto>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Nome);
                ent.Property(a => a.IdentificadorLoja);

                ent.HasMany(e => e.ConfiguracoesProduto)
                   .WithOne(e => e.ConfiguracaoDadosProduto);

                ent.HasMany(e => e.ConfiguracoesCaracteristica)
                   .WithOne(e => e.ConfiguracaoDadosProduto);

                ent.HasMany(e => e.ConfiguracoesImagem)
                   .WithOne(e => e.ConfiguracaoDadosProduto);
            });
        }
    }
}
