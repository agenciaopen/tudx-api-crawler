﻿USE TudXCrawler

DELETE FROM dbo.TratamentoImagem
DBCC CHECKIDENT ('dbo.TratamentoImagem', RESEED, 0);
DELETE FROM dbo.TratamentoProduto
DBCC CHECKIDENT ('dbo.TratamentoProduto', RESEED, 0);
DELETE FROM dbo.ConfiguracaoGenericaCrawler
DBCC CHECKIDENT ('dbo.ConfiguracaoGenericaCrawler', RESEED, 0);
DELETE FROM dbo.ConfiguracaoLojaCrawler
DBCC CHECKIDENT ('dbo.ConfiguracaoLojaCrawler', RESEED, 0);
DELETE FROM dbo.ConfiguracaoDadosProduto
DBCC CHECKIDENT ('dbo.ConfiguracaoDadosProduto', RESEED, 0);
DELETE FROM dbo.ConfiguracaoSplash
DBCC CHECKIDENT ('dbo.ConfiguracaoSplash', RESEED, 0);
DELETE FROM dbo.ConfiguracaoCaracteristica
DBCC CHECKIDENT ('dbo.ConfiguracaoCaracteristica', RESEED, 0);
DELETE FROM dbo.ConfiguracaoCategoria
DBCC CHECKIDENT ('dbo.ConfiguracaoCategoria', RESEED, 0);
DELETE FROM dbo.ConfiguracaoImagem
DBCC CHECKIDENT ('dbo.ConfiguracaoImagem', RESEED, 0);
DELETE FROM dbo.ConfiguracaoListagem
DBCC CHECKIDENT ('dbo.ConfiguracaoListagem', RESEED, 0);
DELETE FROM dbo.ConfiguracaoPaginacao
DBCC CHECKIDENT ('dbo.ConfiguracaoPaginacao', RESEED, 0);
DELETE FROM dbo.ConfiguracaoProduto
DBCC CHECKIDENT ('dbo.ConfiguracaoProduto', RESEED, 0);
DELETE FROM dbo.CaracteristicaCrawler
DBCC CHECKIDENT ('dbo.CaracteristicaCrawler', RESEED, 0);
DELETE FROM dbo.ImagemCrawler
DBCC CHECKIDENT ('dbo.ImagemCrawler', RESEED, 0);
DELETE FROM dbo.ProdutoCrawler
DBCC CHECKIDENT ('dbo.ProdutoCrawler', RESEED, 0);
DELETE FROM dbo.LojaCrawler
DBCC CHECKIDENT ('dbo.LojaCrawler', RESEED, 0);