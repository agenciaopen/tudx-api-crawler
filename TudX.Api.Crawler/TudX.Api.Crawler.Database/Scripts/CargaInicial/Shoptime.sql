﻿USE TudXCrawler

SET IDENTITY_INSERT LojaCrawler ON;

Insert into LojaCrawler (Identificador, Nome, UrlBase)
values (1, 'Shoptime', 'https://www.shoptime.com.br');

SET IDENTITY_INSERT LojaCrawler OFF;

SET IDENTITY_INSERT ConfiguracaoGenericaCrawler ON;

Insert into ConfiguracaoGenericaCrawler (Identificador, Settings, Ativo)
values (1, 'SPLASH_URL = ''http://localhost:8050''', 1);
Insert into ConfiguracaoGenericaCrawler (Identificador, Settings, Ativo)
values (2, 'DUPEFILTER_CLASS = ''scrapy_splash.SplashAwareDupeFilter''', 1);
Insert into ConfiguracaoGenericaCrawler (Identificador, Settings, Ativo)
values (3, 'HTTPCACHE_STORAGE = ''scrapy_splash.SplashAwareFSCacheStorage''', 1);

SET IDENTITY_INSERT ConfiguracaoGenericaCrawler OFF;

SET IDENTITY_INSERT ConfiguracaoLojaCrawler ON;

Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (1, null, 'DEFAULT_REQUEST_HEADERS={\"Accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\",\"Accept-Language\": \"en-US,en;q=0.9\",\"User-Agent\": \"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.172 Safari/537.36 Vivaldi/2.5.1525.46\"}', 1);
Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (2, 1, null, 1);
Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (3, null, 'DOWNLOADER_MIDDLEWARES = {''scrapy_splash.SplashCookiesMiddleware'': 723,''scrapy_splash.SplashMiddleware'': 725,''scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware'': 810}', 1);
Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (4, null, 'SPIDER_MIDDLEWARES = {''scrapy_splash.SplashDeduplicateArgsMiddleware'': 100}', 1);
Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (5, 2, null, 1);
Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (6, 3, null, 1);

SET IDENTITY_INSERT ConfiguracaoLojaCrawler OFF;

SET IDENTITY_INSERT ConfiguracaoSplash ON;

Insert into ConfiguracaoSplash (Identificador, TempoCategoria, TempoListagem, TempoProduto, IdentificadorLoja)
values (1, null, 2, null, 1);

SET IDENTITY_INSERT ConfiguracaoSplash OFF;

SET IDENTITY_INSERT ConfiguracaoCategoria ON;

Insert into ConfiguracaoCategoria (Identificador, UrlBase, UrlSelector, TextSelector, NextSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoCategoriaSuperior)
values (1, 'https://www.shoptime.com.br/mapa-do-site/categoria', null, 'a::text', '.sitemap-block > .sitemap-list > .sitemap-item', null, 1, null);
Insert into ConfiguracaoCategoria (Identificador, UrlBase, UrlSelector, TextSelector, NextSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoCategoriaSuperior)
values (2, null, 'a::attr(href)', 'a::text', '.sitemap-list > .sitemap-item', 'https://www.shoptime.com.br', 1, 1);

SET IDENTITY_INSERT ConfiguracaoCategoria OFF;

SET IDENTITY_INSERT ConfiguracaoListagem ON;

Insert into ConfiguracaoListagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoListagemSuperior)
Values (1, '.product-grid', null, null, 1, null);
Insert into ConfiguracaoListagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoListagemSuperior)
Values (2, '.product-grid-item', 'a::attr(href)', 'https://www.shoptime.com.br', 1, 1);

SET IDENTITY_INSERT ConfiguracaoListagem OFF;

SET IDENTITY_INSERT ConfiguracaoPaginacao ON;

Insert into ConfiguracaoPaginacao (Identificador, NextSelector, UrlSelector, Dominio, ParametroGet, IdentificadorLoja)
Values (1, '.pagination-product-grid > :last-child', 'a::attr(href)', 'https://www.shoptime.com.br', null, 1);

SET IDENTITY_INSERT ConfiguracaoPaginacao OFF;

SET IDENTITY_INSERT ConfiguracaoDadosProduto ON;

Insert into ConfiguracaoDadosProduto (Identificador, Nome, IdentificadorLoja)
Values (1, 'Default', 1);

SET IDENTITY_INSERT ConfiguracaoDadosProduto OFF;

SET IDENTITY_INSERT ConfiguracaoProduto ON;

Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (1, 'CodigoProduto', 'section[class^=\"product-page\"] > div[class^=\"GridUI\"] > div[class^=\"GridUI\"]  > div[class^=\"ColUI\"] > section[class^=\"ViewSection\"] > div[class^=\"GridUI\"] > div[class^=\"ColUI\"] > div[class^=\"ViewUI\"]', 'span[class^=\"TextUI\"]::text', 0, 1, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (2, 'Nome', '[class*=product-title]', '[class*=product-title] ::text', 0, 1, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (3, 'Descricao', '.info-description-frame-inside', '.info-description-frame-inside ::text', 0, 1, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (4, 'Valor', '.sales-price', '.sales-price::text', 0, 1, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (5, 'FormaPagamento', '.installment-wrapper', '.payment-option::text', 0, 1, null);

SET IDENTITY_INSERT ConfiguracaoProduto OFF;

SET IDENTITY_INSERT ConfiguracaoCaracteristica ON;

Insert into ConfiguracaoCaracteristica (Identificador, NextSelector, Nome, Valor, IdentificadorConfiguracaoDadosProduto)
Values (1, 'tbody > tr', 'td:nth-child(1) ::text', 'td:nth-child(2) ::text', 1);

SET IDENTITY_INSERT ConfiguracaoCaracteristica OFF;

SET IDENTITY_INSERT ConfiguracaoImagem ON;

Insert into ConfiguracaoImagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoImagemSuperior)
Values (1, '.image-gallery-slides > .image-gallery-slide', null, null, 1, null);
Insert into ConfiguracaoImagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoImagemSuperior)
Values (2, '.image-gallery-image', 'img::attr(src)', 'https://www.shoptime.com.br', 1, 1);

SET IDENTITY_INSERT ConfiguracaoImagem OFF;

SET IDENTITY_INSERT ConfiguracaoAgendamento ON;

Insert into ConfiguracaoAgendamento (Identificador, IdentificadorLoja, TipoFrequencia, ValorFrequencia, HoraFrequencia, DataInicio, DataFim)
Values (1, 1, 2, 1, '06:00:00', Getdate(), null);

SET IDENTITY_INSERT ConfiguracaoAgendamento OFF;
