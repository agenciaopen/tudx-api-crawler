﻿USE TudXCrawler

SET IDENTITY_INSERT LojaCrawler ON;

Insert into LojaCrawler (Identificador, Nome, UrlBase)
values (2, 'Submarino', 'https://www.submarino.com.br');

SET IDENTITY_INSERT LojaCrawler OFF;


SET IDENTITY_INSERT ConfiguracaoLojaCrawler ON;

Insert into ConfiguracaoLojaCrawler (Identificador, IdentificadorConfiguracaoGenericaCrawler, Settings, IdentificadorLoja)
values (7, null, 'DEFAULT_REQUEST_HEADERS={\"Accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\",\"Accept-Language\": \"en-US,en;q=0.9\",\"User-Agent\": \"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.172 Safari/537.36 Vivaldi/2.5.1525.46\"}', 2);

SET IDENTITY_INSERT ConfiguracaoLojaCrawler OFF;

SET IDENTITY_INSERT ConfiguracaoCategoria ON;

Insert into ConfiguracaoCategoria (Identificador, UrlBase, UrlSelector, TextSelector, NextSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoCategoriaSuperior)
values (3, 'https://www.submarino.com.br/mapa-do-site/categoria', null, 'a::text', '.sitemap-block > .sitemap-list > .sitemap-item', null, 2, null);
Insert into ConfiguracaoCategoria (Identificador, UrlBase, UrlSelector, TextSelector, NextSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoCategoriaSuperior)
values (4, null, 'a::attr(href)', 'a::text', '.sitemap-list > .sitemap-item', 'https://www.submarino.com.br', 2, 3);

SET IDENTITY_INSERT ConfiguracaoCategoria OFF;

SET IDENTITY_INSERT ConfiguracaoListagem ON;

Insert into ConfiguracaoListagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoListagemSuperior)
Values (3, '[data-tracker=\"productgrid-main\"]', null, null, 2, null);
Insert into ConfiguracaoListagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorLoja, IdentificadorConfiguracaoListagemSuperior)
Values (4, '.product-grid-item', 'a::attr(href)', 'https://www.submarino.com.br', 2, 3);

SET IDENTITY_INSERT ConfiguracaoListagem OFF;

SET IDENTITY_INSERT ConfiguracaoPaginacao ON;

Insert into ConfiguracaoPaginacao (Identificador, NextSelector, UrlSelector, Dominio, ParametroGet, IdentificadorLoja)
Values (2, '.pagination-product-grid > :last-child', 'a::attr(href)', 'https://www.submarino.com.br', null, 2);

SET IDENTITY_INSERT ConfiguracaoPaginacao OFF;

SET IDENTITY_INSERT ConfiguracaoDadosProduto ON;

Insert into ConfiguracaoDadosProduto (Identificador, Nome, IdentificadorLoja)
Values (2, 'Default', 2);

SET IDENTITY_INSERT ConfiguracaoDadosProduto OFF;

SET IDENTITY_INSERT ConfiguracaoProduto ON;

Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (6, 'CodigoProduto', '#content', '#content', 0, 2, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (7, 'Nome', '[class*=product-title]', '[class*=product-title] ::text', 0, 2, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (8, 'Descricao', '.info-description-frame-inside', '.info-description-frame-inside ::text', 0, 2, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (9, 'Valor', '.sales-price', '.sales-price::text', 0, 2, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (10, 'FormaPagamento', '.installment-wrapper', '.payment-option::text', 0, 2, null);
Insert into ConfiguracaoProduto (Identificador, NomeCampo, NextSelector, TextSelector, EhColecao, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoProdutoSuperior)
Values (11, 'NomeCategoria', '.product-breadcrumb > div', 'span ::text', 1, 2, null);

SET IDENTITY_INSERT ConfiguracaoProduto OFF;

SET IDENTITY_INSERT TratamentoProduto ON;

Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (1, 2, null, null, '\\(Cód\\.([^\\)]+)\\)', 6);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (2, 1, '-', '', null, 6);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (3, 1, '>', '', null, 6);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (4, 1, '<', '', null, 6);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (5, 1, '!', '', null, 6);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (6, 1, 'R$ ', '', null, 9);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (14, 1, '.', '', null, 9);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (15, 1, ',', '.', null, 9);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (7, 1, '\\r', '', null, 11);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (8, 1, '\\n', '', null, 11);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (9, 1, '\\t', '', null, 11);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (10, 1, '\r', '', null, 11);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (11, 1, '\n', '', null, 11);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (12, 1, '\t', '', null, 11);
Insert into TratamentoProduto (Identificador, Tipo, ValorProcurado, ValorSubstituto, Regex, IdentificadorConfiguracaoProduto)
Values (13, 1, 'Página Inicial', '', null, 11);

SET IDENTITY_INSERT TratamentoProduto OFF;

SET IDENTITY_INSERT ConfiguracaoCaracteristica ON;

Insert into ConfiguracaoCaracteristica (Identificador, NextSelector, Nome, Valor, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoCaracteristicaSuperior)
Values (2, '#info-section', null, 'td:nth-child(2) ::text', 2, null);
Insert into ConfiguracaoCaracteristica (Identificador, NextSelector, Nome, Valor, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoCaracteristicaSuperior)
Values (3, 'tbody > tr', 'td:nth-child(1) ::text', 'td:nth-child(2) ::text', 2, 2);

SET IDENTITY_INSERT ConfiguracaoCaracteristica OFF;

SET IDENTITY_INSERT ConfiguracaoImagem ON;

Insert into ConfiguracaoImagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoImagemSuperior)
Values (3, '.image-gallery-slides > .image-gallery-slide', null, null, 2, null);
Insert into ConfiguracaoImagem (Identificador, NextSelector, UrlSelector, Dominio, IdentificadorConfiguracaoDadosProduto, IdentificadorConfiguracaoImagemSuperior)
Values (4, '.image-gallery-image', 'img::attr(src)', '', 2, 3);

SET IDENTITY_INSERT ConfiguracaoImagem OFF;