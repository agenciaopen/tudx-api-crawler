﻿CREATE TABLE [dbo].[ConfiguracaoProduto] (
    [Identificador]                            BIGINT        IDENTITY (1, 1) NOT NULL,
    [NomeCampo]                                VARCHAR (50)  NOT NULL,
    [NextSelector]                             VARCHAR (255) NULL,
    [TextSelector]                             VARCHAR (255) NULL,
    [EhColecao]                                BIT           NOT NULL,
    [IdentificadorConfiguracaoDadosProduto]    BIGINT        NOT NULL,
    [IdentificadorConfiguracaoProdutoSuperior] BIGINT        NULL,
    CONSTRAINT [PK_ConfiguracaoProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoProduto_ConfiguracaoDadosProduto] FOREIGN KEY ([IdentificadorConfiguracaoDadosProduto]) REFERENCES [dbo].[ConfiguracaoDadosProduto] ([Identificador]),
    CONSTRAINT [FK_ConfiguracaoProduto_ConfiguracaoProduto] FOREIGN KEY ([IdentificadorConfiguracaoProdutoSuperior]) REFERENCES [dbo].[ConfiguracaoProduto] ([Identificador])
);







