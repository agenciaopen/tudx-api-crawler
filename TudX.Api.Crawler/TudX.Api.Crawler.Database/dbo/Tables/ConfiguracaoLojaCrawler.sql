﻿CREATE TABLE [dbo].[ConfiguracaoLojaCrawler] (
    [Identificador]                            BIGINT        IDENTITY (1, 1) NOT NULL,
    [IdentificadorConfiguracaoGenericaCrawler] BIGINT        NULL,
    [Settings]                                VARCHAR (MAX) NULL,
    [IdentificadorLoja]                        BIGINT        NOT NULL,
    CONSTRAINT [PK_ConfiguracaoLojaCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoLojaCrawler_ConfiguracaoGenericaCrawler] FOREIGN KEY ([IdentificadorConfiguracaoGenericaCrawler]) REFERENCES [dbo].[ConfiguracaoGenericaCrawler] ([Identificador]),
    CONSTRAINT [FK_ConfiguracaoLojaCrawler_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);

