﻿CREATE TABLE [dbo].[ConfiguracaoGenericaCrawler] (
    [Identificador] BIGINT        IDENTITY (1, 1) NOT NULL,
    [Settings]      VARCHAR (MAX) NOT NULL,
    [Ativo]         BIT           NOT NULL,
    CONSTRAINT [PK_ConfiguracaoGenericaCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);

