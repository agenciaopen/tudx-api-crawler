﻿CREATE TABLE [dbo].[CaracteristicaCrawler] (
    [Identificador]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]                 VARCHAR (50)  NOT NULL,
    [Valor]                VARCHAR (255) NULL,
    [IdentificadorProduto] BIGINT        NOT NULL,
    CONSTRAINT [PK_CaracteristicaCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_CaracteristicaCrawler_ProdutoCrawler] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[ProdutoCrawler] ([Identificador])
);



