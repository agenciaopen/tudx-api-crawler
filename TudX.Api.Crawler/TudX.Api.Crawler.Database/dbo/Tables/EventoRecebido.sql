﻿CREATE TABLE [dbo].[EventoRecebido] (
    [Identificador]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [IdentificadorEvento] UNIQUEIDENTIFIER NOT NULL,
    [Nome]                VARCHAR (255)    NOT NULL,
    [DataRecebimento]     DATETIME         NOT NULL,
    [Mensagem]            VARCHAR (MAX)    NOT NULL,
    CONSTRAINT [PK_EventoRecebido] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);

