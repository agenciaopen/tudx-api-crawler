﻿CREATE TABLE [dbo].[ConfiguracaoPaginacao] (
    [Identificador]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [NextSelector]      VARCHAR (255) NULL,
    [UrlSelector]       VARCHAR (255) NULL,
    [Dominio]           VARCHAR (255) NULL,
    [ParametroGet]      VARCHAR (255) NULL,
    [IdentificadorLoja] BIGINT        NOT NULL,
    CONSTRAINT [PK_ConfiguracaoPaginacao] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoPaginacao_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);





