﻿CREATE TABLE [dbo].[ConfiguracaoAgendamento] (
    [Identificador]     BIGINT   IDENTITY (1, 1) NOT NULL,
    [IdentificadorLoja] BIGINT   NOT NULL,
    [TipoFrequencia]    SMALLINT NOT NULL,
    [ValorFrequencia]   INT      NULL,
    [HoraFrequencia]    TIME (7) NULL,
    [DataInicio]        DATETIME NOT NULL,
    [DataFim]           DATETIME NULL,
    CONSTRAINT [PK_ConfiguracaoAgendamento] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoAgendamento_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);

