﻿CREATE TABLE [dbo].[ImagemCrawler] (
    [Identificador]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [UrlImagem]            VARCHAR (MAX) NOT NULL,
    [IdentificadorProduto] BIGINT        NOT NULL,
    CONSTRAINT [PK_ImagemCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ImagemCrawler_ProdutoCrawler] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[ProdutoCrawler] ([Identificador])
);

