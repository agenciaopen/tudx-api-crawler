﻿CREATE TABLE [dbo].[LogCrawler] (
    [Identificador]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [IdentificadorLoja] BIGINT        NOT NULL,
    [DataInicio]        DATETIME      NOT NULL,
    [MensagemStatus]    VARCHAR (255) NULL,
    [TipoLog]           SMALLINT      NOT NULL,
    CONSTRAINT [PK_LogCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_LogCrawler_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);

