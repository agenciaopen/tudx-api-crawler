﻿CREATE TABLE [dbo].[ConfiguracaoImagem] (
    [Identificador]                           BIGINT        IDENTITY (1, 1) NOT NULL,
    [NextSelector]                            VARCHAR (255) NULL,
    [UrlSelector]                             VARCHAR (255) CONSTRAINT [DF_ConfiguracaoImagem_EhColecao] DEFAULT ((0)) NULL,
    [Dominio]                                 VARCHAR (255) NULL,
    [IdentificadorConfiguracaoDadosProduto]   BIGINT        NOT NULL,
    [IdentificadorConfiguracaoImagemSuperior] BIGINT        NULL,
    CONSTRAINT [PK_ConfiguracaoImagem] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoImagem_ConfiguracaoDadosProduto] FOREIGN KEY ([IdentificadorConfiguracaoDadosProduto]) REFERENCES [dbo].[ConfiguracaoDadosProduto] ([Identificador]),
    CONSTRAINT [FK_ConfiguracaoImagem_ConfiguracaoImagem] FOREIGN KEY ([IdentificadorConfiguracaoImagemSuperior]) REFERENCES [dbo].[ConfiguracaoImagem] ([Identificador])
);







