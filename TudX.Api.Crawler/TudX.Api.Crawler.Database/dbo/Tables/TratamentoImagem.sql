﻿CREATE TABLE [dbo].[TratamentoImagem] (
    [Identificador]                   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Tipo]                            SMALLINT      NOT NULL,
    [ValorProcurado]                  VARCHAR (255) NULL,
    [ValorSubstituto]                 VARCHAR (255) NULL,
    [IdentificadorConfiguracaoImagem] BIGINT        NOT NULL,
    CONSTRAINT [PK_TratamentoImagem] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_TratamentoImagem_ConfiguracaoImagem] FOREIGN KEY ([IdentificadorConfiguracaoImagem]) REFERENCES [dbo].[ConfiguracaoImagem] ([Identificador])
);

