﻿CREATE TABLE [dbo].[ConfiguracaoSplash] (
    [Identificador]     BIGINT   IDENTITY (1, 1) NOT NULL,
    [TempoCategoria]    SMALLINT NULL,
    [TempoListagem]     SMALLINT NULL,
    [TempoProduto]      SMALLINT NULL,
    [IdentificadorLoja] BIGINT   NOT NULL,
    CONSTRAINT [PK_ConfiguracaoSplash] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoSplash_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);

