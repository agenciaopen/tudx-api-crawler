﻿CREATE TABLE [dbo].[LojaCrawler] (
    [Identificador]			BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]					VARCHAR (255) NOT NULL,
    [UrlBase]				VARCHAR (MAX) NOT NULL,
	[DataUltimoProduto]     DATETIME NULL,
    CONSTRAINT [PK_LojaCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);





