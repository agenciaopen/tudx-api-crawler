﻿CREATE TABLE [dbo].[NotificacaoProdutoCrawler] (
    [Identificador]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [Propriedades]      VARCHAR (255) NOT NULL,
    [DataIdentificacao] DATETIME      NOT NULL,
    [FoiVerificado]     BIT           NOT NULL,
    [Emails]            VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_NotificacaoProdutoCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);

