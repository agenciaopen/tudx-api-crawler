﻿CREATE TABLE [dbo].[ConfiguracaoCaracteristica] (
    [Identificador]                         BIGINT        IDENTITY (1, 1) NOT NULL,
    [NextSelector]                          VARCHAR (255) NOT NULL,
    [Nome]                                  VARCHAR (255) CONSTRAINT [DF_ConfiguracaoCaracteristica_EhColecao] DEFAULT ((0)) NOT NULL,
    [Valor]                                 VARCHAR (255) NOT NULL,
    [IdentificadorConfiguracaoDadosProduto] BIGINT        NOT NULL,
	[IdentificadorConfiguracaoCaracteristicaSuperior] BIGINT NULL
    CONSTRAINT [PK_ConfiguracaoCaracteristica] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoCaracteristica_ConfiguracaoDadosProduto] FOREIGN KEY ([IdentificadorConfiguracaoDadosProduto]) REFERENCES [dbo].[ConfiguracaoDadosProduto] ([Identificador])
);





