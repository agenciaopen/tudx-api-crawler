﻿CREATE TABLE [dbo].[ProdutoCrawler] (
    [Identificador]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [NomeCategoria]     VARCHAR (255)   NOT NULL,
    [CodigoProduto]     VARCHAR (50)    NULL,
    [Nome]              VARCHAR (255)   NOT NULL,
    [Descricao]         TEXT            NOT NULL,
    [UrlAnuncio]        VARCHAR (MAX)   NOT NULL,
    [Valor]             DECIMAL (18, 2) NOT NULL,
    [FormaPagamento]    VARCHAR (50)    NOT NULL,
    [IdentificadorLoja] BIGINT          NOT NULL,
    CONSTRAINT [PK_ProdutoCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ProdutoCrawler_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);

