﻿CREATE TABLE [dbo].[ConfiguracaoDadosProduto] (
    [Identificador]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]              VARCHAR (255) NOT NULL,
    [IdentificadorLoja] BIGINT        NOT NULL,
    CONSTRAINT [PK_ConfiguracaoDadosProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoDadosProduto_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);

