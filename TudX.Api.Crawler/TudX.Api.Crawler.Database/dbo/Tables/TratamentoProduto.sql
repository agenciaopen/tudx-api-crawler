﻿CREATE TABLE [dbo].[TratamentoProduto] (
    [Identificador]                    BIGINT        IDENTITY (1, 1) NOT NULL,
    [Tipo]                             SMALLINT      NOT NULL,
    [ValorProcurado]                   VARCHAR (255) NULL,
    [ValorSubstituto]                  VARCHAR (255) NULL,
    [Regex]                            VARCHAR (255) NULL,
    [IdentificadorConfiguracaoProduto] BIGINT        NOT NULL,
    CONSTRAINT [PK_TratamentoProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_TratamentoProduto_ConfiguracaoProduto] FOREIGN KEY ([IdentificadorConfiguracaoProduto]) REFERENCES [dbo].[ConfiguracaoProduto] ([Identificador])
);

