﻿CREATE TABLE [dbo].[ConfiguracaoCategoria] (
    [Identificador]                              BIGINT        IDENTITY (1, 1) NOT NULL,
    [UrlBase]                                    VARCHAR (MAX) NULL,
    [UrlSelector]                                VARCHAR (255) CONSTRAINT [DF_ConfiguracaoCategoria_EhColecao] DEFAULT ((0)) NULL,
    [TextSelector]                               VARCHAR (255) NULL,
    [NextSelector]                               VARCHAR (255) NULL,
    [Dominio]                                    VARCHAR (255) NULL,
    [IdentificadorLoja]                          BIGINT        NOT NULL,
    [IdentificadorConfiguracaoCategoriaSuperior] BIGINT        NULL,
    CONSTRAINT [PK_ConfiguracaoCategoria] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoCategoria_ConfiguracaoCategoria] FOREIGN KEY ([IdentificadorConfiguracaoCategoriaSuperior]) REFERENCES [dbo].[ConfiguracaoCategoria] ([Identificador]),
    CONSTRAINT [FK_ConfiguracaoCategoria_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);





