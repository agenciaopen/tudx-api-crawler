﻿CREATE TABLE [dbo].[LogErroCrawler] (
    [Identificador]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [IdentificadorLoja] BIGINT        NOT NULL,
    [Spider]            VARCHAR (255) NOT NULL,
    [HttpError]         VARCHAR (255) NULL,
    [Message]           TEXT          NOT NULL,
    [StackTrace]        TEXT          NULL,
    [Data]              DATETIME      NOT NULL,
    CONSTRAINT [PK_LogErrorCrawler] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_LogErroCrawler_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);



