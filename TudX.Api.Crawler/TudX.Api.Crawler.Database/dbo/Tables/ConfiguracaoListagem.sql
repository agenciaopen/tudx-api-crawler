﻿CREATE TABLE [dbo].[ConfiguracaoListagem] (
    [Identificador]                             BIGINT        IDENTITY (1, 1) NOT NULL,
    [NextSelector]                              VARCHAR (255) NULL,
    [UrlSelector]                               VARCHAR (255) CONSTRAINT [DF_ConfiguracaoListagem_EhColecao] DEFAULT ((0)) NULL,
    [Dominio]                                   VARCHAR (255) NULL,
    [IdentificadorLoja]                         BIGINT        NOT NULL,
    [IdentificadorConfiguracaoListagemSuperior] BIGINT        NULL,
    CONSTRAINT [PK_ConfiguracaoListagem] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ConfiguracaoListagem_ConfiguracaoListagem] FOREIGN KEY ([IdentificadorConfiguracaoListagemSuperior]) REFERENCES [dbo].[ConfiguracaoListagem] ([Identificador]),
    CONSTRAINT [FK_ConfiguracaoListagem_LojaCrawler] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[LojaCrawler] ([Identificador])
);





