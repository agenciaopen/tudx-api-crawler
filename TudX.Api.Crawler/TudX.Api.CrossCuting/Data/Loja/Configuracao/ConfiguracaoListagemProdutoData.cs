﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Core.Filters;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoListagemProdutoData
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string UrlSelector { get; set; }
        public string Dominio { get; set; }
        [SwaggerExclude]
        public List<ConfiguracaoListagemProdutoData> Filhos { get; set; }
    }
}
