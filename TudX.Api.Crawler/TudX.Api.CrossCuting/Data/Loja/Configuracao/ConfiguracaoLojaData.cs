﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoLojaData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public List<string> Settings { get; set; }
        public ConfiguracaoCategoriaData Categorias { get; set; }
        public ConfiguracaoListagemPaginacaoProdutoData ListagemEPaginacao { get; set; }
        public List<ConfiguracaoDadosProdutoData> DadosProduto { get; set; }
    }
}
