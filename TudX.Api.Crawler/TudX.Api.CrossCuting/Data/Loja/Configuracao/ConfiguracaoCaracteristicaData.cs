﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Filters;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoCaracteristicaData
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
        [SwaggerExclude]
        public List<ConfiguracaoCaracteristicaData> Filhos { get; set; }
    }
}
