﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model.Enum;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoTratamentoImagemData
    {
        public long Identificador { get; set; }
        public TipoTratamento Tipo { get; set; }
        public string ValorProcurado { get; set; }
        public string ValorSubstituto { get; set; }
    }
}
