﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoPaginacaoProdutoData
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string UrlSelector { get; set; }
        public string Dominio { get; set; }
        public string ParametroGet { get; set; }
    }
}
