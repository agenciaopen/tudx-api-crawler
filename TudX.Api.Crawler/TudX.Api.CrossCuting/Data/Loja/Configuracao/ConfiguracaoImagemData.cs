﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Filters;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoImagemData
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string UrlSelector { get; set; }
        public string Dominio { get; set; }
        [SwaggerExclude]
        public List<ConfiguracaoImagemData> Filhos { get; set; }
        public List<ConfiguracaoTratamentoImagemData> Tratamentos { get; set; }
    }
}
