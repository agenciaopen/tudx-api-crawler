﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoDadosProdutoData
    {
        public string Nome { get; set; }
        public List<ConfiguracaoProdutoData> Campos { get; set; }
        public ConfiguracaoCaracteristicaData Caracteristicas { get; set; }
        public ConfiguracaoImagemData Imagens { get; set; }
    }
}
