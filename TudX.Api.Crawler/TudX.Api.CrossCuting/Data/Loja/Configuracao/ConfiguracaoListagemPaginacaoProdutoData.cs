﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoListagemPaginacaoProdutoData
    {
        public ConfiguracaoSplashData Splash { get; set; }
        public ConfiguracaoListagemProdutoData Listagem { get; set; }
        public ConfiguracaoPaginacaoProdutoData Paginacao { get; set; }
    }
}
