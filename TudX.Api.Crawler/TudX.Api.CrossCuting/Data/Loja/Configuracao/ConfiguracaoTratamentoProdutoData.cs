﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Crawler.Domain.Model.Enum;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoTratamentoProdutoData
    {
        public long Identificador { get; set; }
        public TipoTratamento Tipo { get; set; }
        public string ValorProcurado { get; set; }
        public string ValorSubstituto { get; set; }
        public string Regex { get; set; }
    }
}
