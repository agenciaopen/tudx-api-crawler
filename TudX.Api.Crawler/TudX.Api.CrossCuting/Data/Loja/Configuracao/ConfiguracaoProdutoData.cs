﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Core.Filters;

namespace TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao
{
    public class ConfiguracaoProdutoData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string NextSelector { get; set; }
        public string TextSelector { get; set; }
        public bool EhColecao { get; set; }
        [SwaggerExclude]
        public List<ConfiguracaoProdutoData> Filhos { get; set; }
        public List<ConfiguracaoTratamentoProdutoData> Tratamentos { get; set; }
    }
}
