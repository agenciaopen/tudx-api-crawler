﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data
{
    public class LojaCrawlerData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string UrlBase { get; set; }
    }
}
