﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data
{
    /// <summary>
    /// Dados e entrada das imagens do produto.
    /// </summary>
    public class ImagemCrawlerData
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
        public int Ordem { get; set; }
    }
}
