﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data
{
    /// <summary>
    /// Dados das caracteristicas do produto
    /// </summary>
    public class CaracteristicaCrawlerData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
    }
}
