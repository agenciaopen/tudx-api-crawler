﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.CrossCuting.Data
{
    /// <summary>
    /// Dados do produto crawleado.
    /// </summary>
    public class ProdutoCrawlerData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string NomeCategoria { get; set; }
        public string CodigoProduto { get; set; }
        public string Descricao { get; set; }
        public string UrlAnuncio { get; set; }
        public decimal Valor { get; set; }
        public string FormaPagamento { get; set; }
        public long IdentificadorLoja { get; set; }
        public ICollection<CaracteristicaCrawlerData> Caracteristicas { get; set; }
        public ICollection<ImagemCrawlerData> Imagens { get; set; }
    }
}
