﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Crawler.CrossCuting.Data.LogErroCrawler
{
    public class LogErroCrawlerData
    {
        public long Identificador { get; set; }
        public long IdentificadorLoja { get; set; }
        public string Spider { get; set; }
        public string HttpError { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
