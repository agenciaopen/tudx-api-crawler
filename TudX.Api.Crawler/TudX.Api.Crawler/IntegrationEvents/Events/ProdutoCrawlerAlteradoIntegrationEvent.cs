﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.IntegrationEvents.Events.Data;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Crawler.IntegrationEvents.Events
{
    public class ProdutoCrawlerAlteradoIntegrationEvent : IntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string NomeCategoria { get; set; }
        public string CodigoProduto { get; set; }
        public string Descricao { get; set; }
        public string UrlAnuncio { get; set; }
        public decimal Valor { get; set; }
        public string FormaPagamento { get; set; }
        public long IdentificadorLoja { get; set; }
        public ICollection<CaracteristicaCrawlerDataIntegrationEvent> Caracteristicas { get; set; }
        public ICollection<ImagemCrawlerDataIntegrationEvent> Imagens { get; set; }
    }
}
