﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Crawler.IntegrationEvents.Events.Data
{
    public class ImagemCrawlerDataIntegrationEvent
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
    }
}
