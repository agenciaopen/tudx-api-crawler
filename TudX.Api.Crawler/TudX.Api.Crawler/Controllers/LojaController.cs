﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TudX.Api.Base.Controller;
using TudX.Api.Crawler.Application.Services;
using TudX.Api.Crawler.CrossCuting.Data;
using TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;
using TudX.Specification.Business;

namespace TudX.Api.Crawler.Controllers
{
    /// <summary>
    /// Fornece funcionalidade referentes a entidade Loja
    /// </summary>
    [ApiVersion("1.0")]    
    public class LojaController : TudxApiController
    {
        private LojaCrawlerService lojaCrawlerService;

        /// <summary>
        /// Construtor padrão.
        /// </summary>
        /// <param name="serviceProvider">Provedor de serviço, injeção de dependência.</param>
        public LojaController(IServiceProvider serviceProvider)
        {
            lojaCrawlerService = new LojaCrawlerService(serviceProvider);
        }

        /// <summary>
        /// Recupera todas as lojas.
        /// </summary>
        /// <response code="200">Dados de todas as lojas</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [ProducesResponseType(typeof(LojaCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> Buscar()
        {
            var lojas = lojaCrawlerService.BuscarTodasAsLojas();

            return Ok(lojas.Adapt<List<LojaCrawlerData>>());
        }

        /// <summary>
        /// Recupera a loja com o identificador passado como parâmetro.
        /// </summary>
        /// <param name="identificador">Identificador da loja desejada.</param>
        /// <response code="200">Dados da lojas/response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [Route("{identificador}")]
        [ProducesResponseType(typeof(LojaCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarPorIdentificador(long identificador)
        {
            var loja = lojaCrawlerService.BuscarLojaPorIdentificador(identificador);

            return Ok(loja.Adapt<LojaCrawlerData>());
        }


        /// <summary>
        /// Insere uma nova loja.
        /// </summary>
        /// <response code="200">Dados da lojas</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="lojaCrawlerData">Dados da loja que será inserida.</param>
        /// <returns>Dados da loja inserida.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(LojaCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> Salvar([FromBody] LojaCrawlerData lojaCrawlerData)
        {
            var loja = lojaCrawlerService.Salvar(lojaCrawlerData.Adapt<LojaCrawler>());            

            return CreatedAtAction(nameof(BuscarPorIdentificador), new { identificador = loja.Identificador }, loja);
        }

        /// <summary>
        /// Recupera a configuração da loja com o identificador passado como parâmetro.
        /// </summary>
        /// <param name="identificador">Identificador da loja desejada.</param>
        /// <response code="200">Dados da configuração da loja/response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [Route("configuracao/{identificador}")]
        [ProducesResponseType(typeof(ConfiguracaoLojaData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarConfiguracao(long identificador)
        {
            return Ok(lojaCrawlerService.BuscarConfiguracaoDaLoja(identificador));
        }
    }
}