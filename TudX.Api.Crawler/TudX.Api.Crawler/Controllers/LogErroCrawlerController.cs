﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TudX.Api.Base.Controller;
using TudX.Api.Crawler.Application.Services;
using TudX.Api.Crawler.CrossCuting.Data.LogErroCrawler;
using TudX.Api.Crawler.Domain.Model;
using TudX.Exceptions;

namespace TudX.Api.Crawler.Controllers
{
    /// <summary>
    /// Fornece funcionalidade referentes aos logs da api.
    /// </summary>
    [ApiVersion("1.0")]
    public class LogErroCrawlerController : TudxApiController
    {
        private readonly LogErroCrawlerService logErroCrawlerService;

        /// <summary>
        /// Construtor padrão.
        /// </summary>
        /// <param name="serviceProvider"></param>
        public LogErroCrawlerController(IServiceProvider serviceProvider)
        {
            logErroCrawlerService = new LogErroCrawlerService(serviceProvider);
        }

        /// <summary>
        /// Recupera o log erro do crawler com o identificador passado como parâmetro.
        /// </summary>
        /// <param name="identificador">Identificador dlog  desejada.</param>
        /// <response code="200">Dados da lojas/response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [Route("{identificador}")]
        [ProducesResponseType(typeof(LogErroCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarPorIdentificador(long identificador)
        {
            var log = logErroCrawlerService.BuscarLogErroCrawlerPorIdentificador(identificador);

            return Ok(log.Adapt<LogErroCrawlerData>());
        }

        /// <summary>
        /// Insere um novo log de erro do crawler.
        /// </summary>
        /// <response code="200">Dados do log de erro do crawler</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="lojaCrawlerData">Dados do log de erro do crawler que será inserido.</param>
        /// <returns>Dados do log de erro inserido.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(LogErroCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> LogErroCrawler([FromBody] LogErroCrawlerData lojaCrawlerData)
        {
            var log = logErroCrawlerService.Salvar(lojaCrawlerData.Adapt<LogErroCrawler>());

            return CreatedAtAction(nameof(BuscarPorIdentificador), new { identificador = log.Identificador }, log);
        }
    }
}