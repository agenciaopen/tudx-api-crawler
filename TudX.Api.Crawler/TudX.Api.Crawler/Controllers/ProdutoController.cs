﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TudX.Api.Base.Controller;
using TudX.Api.Crawler.Application.Services;
using TudX.Api.Crawler.CrossCuting.Data;
using TudX.Api.Crawler.Domain.Model;
using TudX.Api.Crawler.IntegrationEvents.Events;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Crawler.Controllers
{
    /// <summary>
    /// Fornece funcionalidade referentes a entidade Loja
    /// </summary>
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class ProdutoController : TudxApiController
    {
        private ProdutoCrawlerService produtoCrawlerService;
        private CaracteristicaCrawlerService caracteristicaCrawlerService;
        private ImagemCrawlerService imagemCrawlerService;
        private IEventBus eventBus;

        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="serviceProvider">Provedor de serviço, injeção de dependência.</param>
        /// <param name="eventBus">Implementação da fila de mensagens</param>
        public ProdutoController(IServiceProvider serviceProvider,
                                 IEventBus eventBus)
        {
            produtoCrawlerService = new ProdutoCrawlerService(serviceProvider);
            caracteristicaCrawlerService = new CaracteristicaCrawlerService(serviceProvider);
            imagemCrawlerService = new ImagemCrawlerService(serviceProvider);
            this.eventBus = eventBus;
        }

        /// <summary>
        /// Insere ou altera uma produto crawleado.
        /// </summary>
        /// <response code="200">Verdadeiro para quando o salvar ocorrer corretamente.</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="produtoCrawlerData">Dados do produto que será incluído ou alterado.</param>
        /// <returns>Verdadeiro para sucesso no comportamento de salvar.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ProdutoCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<bool>> Salvar([FromBody] ProdutoCrawlerData produtoCrawlerData)
        {
            try
            {
                Tuple<ProdutoCrawler, bool> entidade = produtoCrawlerService.Salvar(produtoCrawlerData);
                CriarMensagemDoProduto(entidade.Item1, entidade.Item2);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Insere ou altera uma lista de produtos crawleado.
        /// </summary>
        /// <response code="200">Verdadeiro para quando o salvar ocorrer corretamente.</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="produtosCrawlerData">Lista de dados do produto que será incluído ou alterado.</param>
        /// <returns>Verdadeiro para sucesso no comportamento de salvar.</returns>
        [HttpPost]
        [Route("Lista")]
        [ProducesResponseType(typeof(ProdutoCrawlerData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<bool>> SalvarVarios([FromBody] List<ProdutoCrawlerData> produtosCrawlerData)
        {
            try
            {
                foreach (var produto in produtosCrawlerData)
                {
                    Tuple<ProdutoCrawler, bool> entidade = produtoCrawlerService.Salvar(produto);
                    CriarMensagemDoProduto(entidade.Item1, entidade.Item2);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Recria as mensagens
        /// </summary>
        /// <param name="identificadorLoja">Identificador da loja</param>
        /// <returns>Verdadeiro para mensagem criada com sucesso, caso contrário falso.</returns>
        [HttpGet]
        [Route("Loja/RecriaMensagem/{identificadorLoja}")]
        [ProducesResponseType(typeof(Boolean), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<bool>> RecriarMensagensPorLoja(long identificadorLoja)
        {
            foreach (var produto in produtoCrawlerService.BuscarTodosOsProdutosDaLoja(identificadorLoja).OrderByDescending(p => p.Identificador))
            {
                produto.Caracteristicas = caracteristicaCrawlerService.BuscarCaracteristicasCrawlerDoProduto(produto.Identificador);
                produto.Imagens = imagemCrawlerService.BuscarImagemCrawlerDoProduto(produto.Identificador);
                CriarMensagemDoProduto(produto, true);
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identificadorLoja"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Loja/CorrigeUrl/{identificadorLoja}")]
        [ProducesResponseType(typeof(Boolean), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<bool>> CorrigeUrlImagem(long identificadorLoja)
        {
            foreach (var imagem in imagemCrawlerService.BuscarImagemCrawlerDosProdutosDaLoja(identificadorLoja))
            {
                imagem.UrlImagem = imagem.UrlImagem;
                imagemCrawlerService.Save(imagem);
                imagemCrawlerService.SaveChanges();
            }

            return true;
        }

        private void CriarMensagemDoProduto(ProdutoCrawler produtoCrawler, bool isUpdate)
        {
            if (isUpdate)
            {
                ProdutoCrawlerAlteradoIntegrationEvent message = produtoCrawler.Adapt<ProdutoCrawlerAlteradoIntegrationEvent>();                
                eventBus.PublishAndRegisterEvent(message);
            }
            else
            {
                ProdutoCrawlerAdicionadoIntegrationEvent message = produtoCrawler.Adapt<ProdutoCrawlerAdicionadoIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
        }
    }
}