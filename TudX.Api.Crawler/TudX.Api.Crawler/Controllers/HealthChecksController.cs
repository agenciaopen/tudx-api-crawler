﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TudX.Api.Base.Controller;

namespace TudX.Api.Crawler.Controllers
{
    /// <summary>
    /// Verificador de saúde da api.
    /// </summary>
    [ApiVersion("1.0")]    
    public class HealthChecksController : TudxHealthChecksController
    {
    }
}