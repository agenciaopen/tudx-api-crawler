﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TudX.Adapter.EntityFramework.Context;
using TudX.Api.Base;
using TudX.Api.Base.Metadata;
using TudX.Api.Crawler.Infra.EF.Context;
using TudX.Domain.Base.Repository;
using TudX.Repository.Base;
using TudX.Api.Crawler.Infra.EF.DependencyInjection;
using TudX.Adapter.Eventbus.RabbitMQ.Extensions;

namespace TudX.Api.Crawler
{
    /// <summary>
    /// Configuração de inicio da aplicação.
    /// </summary>
    public class Startup : TudxApiBaseStartup
    {
        /// <summary>
        /// Informações a serem apresentadas na documentação do swagger.
        /// </summary>
        protected override ApiMetadata ApiMetadata => new ApiMetadata()
        {
            Name = "Api Crawler",
            Description = "Esta api será utilizada pelo recurso de crawler.",
            DefaultApiVersion = "1.0"
        };

        /// <summary>
        /// Construtor.
        /// </summary>
        /// <param name="configuration">Informações de configurações da aplicação.</param>
        public Startup(IConfiguration configuration) : base(configuration)
        {
            
        }

        /// <summary>
        /// Métodos de configurações de IoC
        /// </summary>
        /// <param name="services">Classe responsável por configurar o DI.</param>
        protected override void ConfigureApiServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();            
            services.AddCors(options =>
            {
                options.AddPolicy("TudXPolicy",
                builder =>
                {
                    builder.WithOrigins("http://tudx01",
                                        "http://localhost",
                                        "http://localhost:4200")
                                        .AllowAnyHeader()
                                        .AllowAnyMethod()
                                        .AllowCredentials();
                });
            });

            services.AddEFAdapter(Configuration);
            services.AddRabbitMQAdapter(Configuration);            
        }


        /// <summary>
        /// Método de configuração da aplicação.
        /// </summary>
        /// <param name="app">Construtor da aplicação.</param>
        protected override void ConfigureApi(IApplicationBuilder app)
        {
            app.UseCors("TudXPolicy");
            app.UseRouting();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
