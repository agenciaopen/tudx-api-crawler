﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ConfiguracaoAgendamentoPorLojaSpecification : SpecificationBase<ConfiguracaoAgendamento>
    {
        private long identificadorLoja;

        public ConfiguracaoAgendamentoPorLojaSpecification(long identificadorLoja)
        {
            this.identificadorLoja = identificadorLoja;
        }

        public override Expression<Func<ConfiguracaoAgendamento, bool>> SatisfiedBy()
        {
            return c => c.IdentificadorLoja == identificadorLoja;
        }
    }
}
