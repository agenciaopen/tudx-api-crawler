﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification.Produto
{
    public class ProdutoPorLojaSpecification : SpecificationBase<ProdutoCrawler>
    {
        private long identificadorLoja;

        public ProdutoPorLojaSpecification(long identificadorLoja)
        {
            this.identificadorLoja = identificadorLoja;
        }

        public override Expression<Func<ProdutoCrawler, bool>> SatisfiedBy()
        {
            return p => p.IdentificadorLoja == identificadorLoja;
        }
    }
}
