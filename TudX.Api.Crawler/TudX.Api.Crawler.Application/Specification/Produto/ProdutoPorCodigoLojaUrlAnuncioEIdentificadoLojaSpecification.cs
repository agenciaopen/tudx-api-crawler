﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification.Produto
{
    public class ProdutoPorCodigoLojaUrlAnuncioEIdentificadoLojaSpecification : SpecificationBase<ProdutoCrawler>
    {
        private string codigoProduto;

        private long identificadorLoja;

        private string urlAnuncio;

        public ProdutoPorCodigoLojaUrlAnuncioEIdentificadoLojaSpecification(string codigoProduto, long identificadorLoja, string urlAnuncio)
        {
            this.codigoProduto = codigoProduto;
            this.identificadorLoja = identificadorLoja;
            this.urlAnuncio = urlAnuncio;
        }

        public override Expression<Func<ProdutoCrawler, bool>> SatisfiedBy()
        {
            return p => p.CodigoProduto == codigoProduto && p.IdentificadorLoja == identificadorLoja && p.UrlAnuncio.Trim() == urlAnuncio.Trim();
        }
    }
}
