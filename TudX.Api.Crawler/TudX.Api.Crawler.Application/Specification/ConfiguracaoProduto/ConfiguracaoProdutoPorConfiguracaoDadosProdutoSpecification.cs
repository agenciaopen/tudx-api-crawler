﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ConfiguracaoProdutoPorConfiguracaoDadosProdutoSpecification : SpecificationBase<ConfiguracaoProduto>
    {
        private long identificadorConfiguracaoDadosProduto;

        public ConfiguracaoProdutoPorConfiguracaoDadosProdutoSpecification(long identificador)
        {
            this.identificadorConfiguracaoDadosProduto = identificador;
        }

        public override Expression<Func<ConfiguracaoProduto, bool>> SatisfiedBy()
        {
            return c => c.ConfiguracaoDadosProduto.Identificador == identificadorConfiguracaoDadosProduto;
        }
    }
}
