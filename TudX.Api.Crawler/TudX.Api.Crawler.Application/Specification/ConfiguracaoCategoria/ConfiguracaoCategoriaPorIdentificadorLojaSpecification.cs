﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ConfiguracaoCategoriaPorIdentificadorLojaSpecification : SpecificationBase<ConfiguracaoCategoria>
    {
        private long identificadorLoja;

        public ConfiguracaoCategoriaPorIdentificadorLojaSpecification(long identificador)
        {
            this.identificadorLoja = identificador;
        }

        public override Expression<Func<ConfiguracaoCategoria, bool>> SatisfiedBy()
        {
            return c => c.Loja.Identificador == identificadorLoja;
        }
    }
}
