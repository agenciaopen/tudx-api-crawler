﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ConfiguracaoImagemPorConfiguracaoDadosProdutoSpecification : SpecificationBase<ConfiguracaoImagem>
    {
        private long identificadorConfiguracaoDadosProduto;

        public ConfiguracaoImagemPorConfiguracaoDadosProdutoSpecification(long identificador)
        {
            this.identificadorConfiguracaoDadosProduto = identificador;
        }

        public override Expression<Func<ConfiguracaoImagem, bool>> SatisfiedBy()
        {
            return c => c.ConfiguracaoDadosProduto.Identificador == identificadorConfiguracaoDadosProduto;
        }
    }
}
