﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class CaracteristicaCrawlerPorProdutoSpecification : SpecificationBase<CaracteristicaCrawler>
    {
        private long identificadorProduto;

        public CaracteristicaCrawlerPorProdutoSpecification(long identificadorProduto)
        {
            this.identificadorProduto = identificadorProduto;
        }

        public override Expression<Func<CaracteristicaCrawler, bool>> SatisfiedBy()
        {
            return c => c.Produto.Identificador == identificadorProduto;
        }
    }
}
