﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ImagemCrawlerPorProdutoSpecification : SpecificationBase<ImagemCrawler>
    {
        private long identificadorProduto;

        public ImagemCrawlerPorProdutoSpecification(long identificadorProduto)
        {
            this.identificadorProduto = identificadorProduto;
        }

        public override Expression<Func<ImagemCrawler, bool>> SatisfiedBy()
        {
            return c => c.Produto.Identificador == identificadorProduto;
        }
    }
}
