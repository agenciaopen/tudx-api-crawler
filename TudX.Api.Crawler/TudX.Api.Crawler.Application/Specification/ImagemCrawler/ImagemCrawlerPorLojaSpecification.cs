﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ImagemCrawlerPorLojaSpecification : SpecificationBase<ImagemCrawler>
    {
        private long identificadorLoja;

        public ImagemCrawlerPorLojaSpecification(long identificadorLoja)
        {
            this.identificadorLoja = identificadorLoja;
        }

        public override Expression<Func<ImagemCrawler, bool>> SatisfiedBy()
        {
            return c => c.Produto.IdentificadorLoja == identificadorLoja;
        }
    }
}
