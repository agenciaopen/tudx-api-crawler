﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ConfiguracaoSplashPorLojaSpecification : SpecificationBase<ConfiguracaoSplash>
    {
        private long identificadorLoja;

        public ConfiguracaoSplashPorLojaSpecification(long identificadorLoja)
        {
            this.identificadorLoja = identificadorLoja;
        }

        public override Expression<Func<ConfiguracaoSplash, bool>> SatisfiedBy()
        {
            return c => c.IdentificadorLoja == identificadorLoja;
        }
    }
}
