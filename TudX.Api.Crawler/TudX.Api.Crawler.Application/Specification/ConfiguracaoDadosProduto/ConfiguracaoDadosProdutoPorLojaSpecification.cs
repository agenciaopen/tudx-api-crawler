﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Crawler.Application.Specification
{
    public class ConfiguracaoDadosProdutoPorLojaSpecification : SpecificationBase<ConfiguracaoDadosProduto>
    {
        private long identificadorLoja;

        public ConfiguracaoDadosProdutoPorLojaSpecification(long identificadorLoja)
        {
            this.identificadorLoja = identificadorLoja;
        }

        public override Expression<Func<ConfiguracaoDadosProduto, bool>> SatisfiedBy()
        {
            return c => c.IdentificadorLoja == identificadorLoja;
        }
    }
}
