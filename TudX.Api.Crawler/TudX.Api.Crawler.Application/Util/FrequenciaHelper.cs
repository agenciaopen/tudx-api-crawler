﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model.Enum;

namespace TudX.Api.Crawler.Application.Util
{
    public static class FrequenciaHelper
    {
        public static bool ValidaSeAFrequenciaFoiAtingida(TipoFrequencia tipoFrequencia, int? valorFrequencia, TimeSpan? horaFrequencia, DateTime dataUltimoProduto)
        {
            switch (tipoFrequencia)
            {
                case TipoFrequencia.Hora:
                    return CalcularFrequenciaPorHora(valorFrequencia, dataUltimoProduto);
                case TipoFrequencia.Dia:
                    return CalcularFrequenciaPorDia(valorFrequencia, horaFrequencia, dataUltimoProduto);
            }

            return false;
        }

        private static bool CalcularFrequenciaPorDia(int? valorFrequencia, TimeSpan? horaFrequencia, DateTime dataUltimoProduto)
        {
            DateTime dataAtual = DateTime.Now;
            double dias = (dataAtual - dataUltimoProduto).TotalDays;
            
            if (dias > valorFrequencia.Value)
                return true;

            if (dias == valorFrequencia.Value && horaFrequencia.HasValue)
            {
                DateTime dataHoraExecucao =
                    new DateTime(dataAtual.Year, dataAtual.Month, dataAtual.Day, horaFrequencia.Value.Hours, horaFrequencia.Value.Minutes, horaFrequencia.Value.Seconds);

                if (dataAtual > dataHoraExecucao && dataHoraExecucao > dataUltimoProduto)
                    return true;
            }

            return false;
        }

        private static bool CalcularFrequenciaPorHora(int? valorFrequencia, DateTime dataUltimoProduto)
        {
            DateTime dataAtual = DateTime.Now;
            double horas = (dataAtual - dataUltimoProduto).TotalHours;
            if (valorFrequencia.HasValue)
            {
                if (horas >= valorFrequencia.Value)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
