﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoListagemService : DomainServiceRelationalBase<ConfiguracaoListagem>
    {
        public ConfiguracaoListagemService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        #region Includes
        public Expression<Func<ConfiguracaoListagem, object>>[] ConfiguracaoListagemIncludesFilho()
        {
            return new Expression<Func<ConfiguracaoListagem, object>>[] { p => p.Filhos };
        }
        #endregion
    }
}
