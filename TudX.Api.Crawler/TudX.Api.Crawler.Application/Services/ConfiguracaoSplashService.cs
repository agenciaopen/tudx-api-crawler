﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoSplashService : DomainServiceRelationalBase<ConfiguracaoSplash>
    {
        public ConfiguracaoSplashService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }
    }
}
