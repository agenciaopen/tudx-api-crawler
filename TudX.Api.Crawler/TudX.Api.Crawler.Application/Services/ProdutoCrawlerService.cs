﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TudX.Api.Crawler.Application.Specification.Produto;
using TudX.Api.Crawler.Domain.Model;
using TudX.Api.Crawler.CrossCuting.Data;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;
using TudX.IntegrationEvents.EventBus.Abstractions;
using System.Linq.Expressions;
using TudX.Core.Helpers;
using System.Reflection;

namespace TudX.Api.Crawler.Application.Services
{
    /// <summary>
    /// Domain service da entidade ProdutoCrawler
    /// </summary>
    public class ProdutoCrawlerService : DomainServiceRelationalBase<ProdutoCrawler>
    {
        private CaracteristicaCrawlerService caracteristicaCrawlerService;
        private ImagemCrawlerService imagemCrawlerService;
        private LojaCrawlerService lojaCrawlerService;

        public ProdutoCrawlerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            this.caracteristicaCrawlerService = new CaracteristicaCrawlerService(serviceProvider);
            this.imagemCrawlerService = new ImagemCrawlerService(serviceProvider);
            this.lojaCrawlerService = new LojaCrawlerService(serviceProvider);
        }

        /// <summary>
        /// Salva o produto crawleado.
        /// </summary>
        /// <param name="produtoCrawlerData">Produto crawleado.</param>
        /// <returns>Produto criado/alterado.</returns>
        public Tuple<ProdutoCrawler, bool> Salvar(ProdutoCrawlerData produtoCrawlerData)
        {
            bool isUpdate = true;
            ProdutoCrawler produtoCrawler 
                = this.FindSingleBySpecification(new ProdutoPorCodigoLojaUrlAnuncioEIdentificadoLojaSpecification(produtoCrawlerData.CodigoProduto, 
                                                                                                  produtoCrawlerData.IdentificadorLoja,
                                                                                                  produtoCrawlerData.UrlAnuncio), 
                                                IncludesCaracteristicasEImagens(), true);            

            if (produtoCrawler == null)
            {
                isUpdate = false;
                produtoCrawler = produtoCrawlerData.Adapt<ProdutoCrawler>();                
            } else
            {
                AplicarAlteracaoNoProduto(produtoCrawler, produtoCrawlerData);             
            }

            this.Save(produtoCrawler);

            AtualizarDataDoUltimoProdutoCrawleadoDaLoja(produtoCrawlerData.IdentificadorLoja);

            this.SaveChanges();

            return new Tuple<ProdutoCrawler, bool>(produtoCrawler, isUpdate);
        }

        /// <summary>
        /// Busca todos os produto da loja
        /// </summary>
        /// <param name="identificadorLoja">Identificador da loja</param>
        /// <returns></returns>
        public List<ProdutoCrawler> BuscarTodosOsProdutosDaLoja(long identificadorLoja)
        {
            IEnumerable<ProdutoCrawler> produtosCrawler = this.FindBySpecification(new ProdutoPorLojaSpecification(identificadorLoja));

            if (produtosCrawler == null)
                return new List<ProdutoCrawler>();

            return produtosCrawler.ToList();
        }

        #region Métodos privados
        private void AtualizarDataDoUltimoProdutoCrawleadoDaLoja(long identificadorLoja)
        {
            LojaCrawler loja = this.lojaCrawlerService.FindById(identificadorLoja);
            loja.DataUltimoProduto = DateTime.Now;

            this.lojaCrawlerService.Save(loja);
        }

        private void AplicarAlteracaoNoProduto(ProdutoCrawler produtoCrawler, ProdutoCrawlerData produtoCrawlerData)
        {
            produtoCrawler.Descricao = produtoCrawlerData.Descricao;
            produtoCrawler.FormaPagamento = produtoCrawlerData.FormaPagamento;            
            produtoCrawler.Nome = produtoCrawlerData.Nome;
            produtoCrawler.NomeCategoria = produtoCrawlerData.NomeCategoria;
            produtoCrawler.UrlAnuncio = produtoCrawlerData.UrlAnuncio;
            produtoCrawler.Valor = produtoCrawlerData.Valor;
            AplicarAlteracaoCaracteristicas(produtoCrawler, produtoCrawlerData);
            AplicarAlteracaoImagem(produtoCrawler, produtoCrawlerData);
        }

        private void AplicarAlteracaoImagem(ProdutoCrawler produtoCrawler, ProdutoCrawlerData produtoCrawlerData)
        {
            if (produtoCrawlerData.Imagens == null && produtoCrawlerData.Imagens.Count() <= 0)
                return;

            if (produtoCrawler.Imagens == null && produtoCrawler.Imagens.Count() <= 0)
            {
                produtoCrawler.Imagens = produtoCrawlerData.Imagens.Adapt<List<ImagemCrawler>>();
                return;
            }

            List<ImagemCrawler> imagensCrawler = produtoCrawler.Imagens.ToList();
            List<ImagemCrawlerData> imagensCrawlerData = produtoCrawlerData.Imagens.ToList();
            List<ImagemCrawler> imagensRemovidas = imagensCrawler.Where(c => !produtoCrawlerData.Imagens.Any(pc => pc.UrlImagem == c.UrlImagem)).ToList();
            imagensRemovidas.ForEach(i =>
            {
                imagensCrawler.Remove(i);
                imagemCrawlerService.Delete(i);
            });

            foreach (var imagem in produtoCrawler.Imagens)
            {
                var imagemVO = imagensCrawlerData.FirstOrDefault(c => c.UrlImagem == imagem.UrlImagem);
                if (imagemVO != null)
                {
                    imagensCrawlerData.Remove(imagemVO);
                }
            }

            foreach (var addImagem in imagensCrawlerData)
            {
                ImagemCrawler imagemCrawlerAdd = addImagem.Adapt<ImagemCrawler>();
                imagemCrawlerAdd.Produto = produtoCrawler;
                imagensCrawler.Add(imagemCrawlerAdd);                
            }

            produtoCrawler.Imagens = imagensCrawler;
        }

        private void AplicarAlteracaoCaracteristicas(ProdutoCrawler produtoCrawler, ProdutoCrawlerData produtoCrawlerData)
        {
            if (produtoCrawlerData.Caracteristicas == null && produtoCrawlerData.Caracteristicas.Count() <= 0)
                return;

            if (produtoCrawler.Caracteristicas == null && produtoCrawler.Caracteristicas.Count() <= 0)
            {
                produtoCrawler.Caracteristicas = produtoCrawlerData.Caracteristicas.Adapt<List<CaracteristicaCrawler>>();
                return;
            }

            List<CaracteristicaCrawler> caracteristicasCrawler = produtoCrawler.Caracteristicas.ToList();
            List<CaracteristicaCrawlerData> caracteristicasCrawlerData = produtoCrawlerData.Caracteristicas.ToList();
            List<CaracteristicaCrawler> caracteristicasRemovidas = caracteristicasCrawler.Where(c => !produtoCrawlerData.Caracteristicas.Any(pc => pc.Nome == c.Nome)).ToList();
            caracteristicasRemovidas.ForEach(cr =>
            {
                caracteristicasCrawler.Remove(cr);
                caracteristicaCrawlerService.Delete(cr);
            });

            foreach (var caracteristica in caracteristicasCrawler)
            {
                var caracteristicaVO = caracteristicasCrawlerData.FirstOrDefault(c => c.Nome == caracteristica.Nome);
                if (caracteristicaVO != null)
                {
                    caracteristica.Valor = caracteristicaVO.Valor;
                    caracteristicasCrawlerData.Remove(caracteristicaVO);
                }
            }

            foreach (var addCaracteristica in caracteristicasCrawlerData)
            {
                CaracteristicaCrawler caracteristicaCrawlerAdd = addCaracteristica.Adapt<CaracteristicaCrawler>();
                caracteristicaCrawlerAdd.Produto = produtoCrawler;
                caracteristicasCrawler.Add(caracteristicaCrawlerAdd);                
            }

            produtoCrawler.Caracteristicas = caracteristicasCrawler;
        }

        private Expression<Func<ProdutoCrawler, object>>[] IncludesCaracteristicasEImagens()
        {
            return new Expression<Func<ProdutoCrawler, object>>[] { p => p.Caracteristicas, p => p.Imagens };
        }
        #endregion
    }
}
