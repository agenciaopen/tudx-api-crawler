﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoProdutoService : DomainServiceRelationalBase<ConfiguracaoProduto>
    {
        public ConfiguracaoProdutoService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        #region Includes
        public Expression<Func<ConfiguracaoProduto, object>>[] ConfiguracaoProdutoIncludesFilhosETratamentos()
        {
            return new Expression<Func<ConfiguracaoProduto, object>>[] { p => p.Filhos, p => p.Tratamentos };
        }
        #endregion
    }
}
