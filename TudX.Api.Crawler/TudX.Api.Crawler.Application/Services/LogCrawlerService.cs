﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Api.Crawler.Domain.Model.Enum;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class LogCrawlerService : DomainServiceRelationalBase<LogCrawler>
    {
        public LogCrawlerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public void RegistrarInicioDoCrawler(long identificadorLoja, string mensagemStatus, TipoLog tipoLog = TipoLog.Sucesso)
        {
            LogCrawler logCrawler = new LogCrawler();
            logCrawler.IdentificadorLoja = identificadorLoja;
            logCrawler.DataInicio = DateTime.Now;
            logCrawler.MensagemStatus = mensagemStatus;
            logCrawler.TipoLog = tipoLog;

            this.Save(logCrawler);
            this.SaveChanges();
        }
    }
}
