﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using TudX.Api.Crawler.Application.Specification;
using TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;
using TudX.Extensions;
using TudX.Specification.Business;

namespace TudX.Api.Crawler.Application.Services
{
    /// <summary>
    /// Domain service da entidade Loja.
    /// </summary>
    public class LojaCrawlerService : DomainServiceRelationalBase<LojaCrawler>
    {
        private ConfiguracaoCategoriaService configuracaoCategoriaService;
        private ConfiguracaoPaginacaoService configuracaoPaginacaoService;
        private ConfiguracaoListagemService configuracaoListagemService;
        private ConfiguracaoProdutoService configuracaoProdutoService;
        private ConfiguracaoCaracteristicaService configuracaoCaracteristicaService;
        private ConfiguracaoImagemService configuracaoImagemService;
        private ConfiguracaoSplashService configuracaoSplashService;
        private ConfiguracaoDadosProdutoService configuracaoDadosProdutoService;
        private ConfiguracaoGenericaCrawlerService configuracaoGenericaCrawlerService;

        public LojaCrawlerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            configuracaoCategoriaService = new ConfiguracaoCategoriaService(serviceProvider);
            configuracaoPaginacaoService = new ConfiguracaoPaginacaoService(serviceProvider);
            configuracaoListagemService = new ConfiguracaoListagemService(serviceProvider);
            configuracaoProdutoService = new ConfiguracaoProdutoService(serviceProvider);
            configuracaoCaracteristicaService = new ConfiguracaoCaracteristicaService(serviceProvider);
            configuracaoImagemService = new ConfiguracaoImagemService(serviceProvider);
            configuracaoSplashService = new ConfiguracaoSplashService(serviceProvider);
            configuracaoDadosProdutoService = new ConfiguracaoDadosProdutoService(serviceProvider);
            configuracaoGenericaCrawlerService = new ConfiguracaoGenericaCrawlerService(serviceProvider);
        }

        public LojaCrawler Salvar(LojaCrawler lojaCrawler)
        {
            this.Save(lojaCrawler);
            this.SaveChanges();

            return lojaCrawler;
        }

        public List<LojaCrawler> BuscarTodasAsLojas()
        {
            return this.FindAll().ToList();
        }

        /// <summary>
        /// Busca a loja pelo o identificador
        /// </summary>
        /// <param name="identificador">Identificador da loja</param>
        /// <returns>Loja encontrada.</returns>
        public LojaCrawler BuscarLojaPorIdentificador(long identificador)
        {
            var loja = this.FindById(identificador, IncludesConfiguracoes());

            if (loja == null)
                throw new BusinessException("Não foi localizado loja para o identificador informado.");

            return loja;
        }

        /// <summary>
        /// Buscar as configurações da loja.
        /// </summary>
        /// <param name="identificador">Identificador da loja</param>
        /// <returns>Configuração da loja</returns>
        public ConfiguracaoLojaData BuscarConfiguracaoDaLoja(long identificador)
        {
            LojaCrawler lojaCrawler = BuscarLojaPorIdentificador(identificador);
            ConfiguracaoLojaData configuracaoLojaData = lojaCrawler.Adapt<ConfiguracaoLojaData>();

            configuracaoLojaData.Settings = GerarSettings(lojaCrawler.Configuracoes);
            configuracaoLojaData.Categorias = BuscarConfiguracaoDaCategoriaDaLoja(identificador);
            configuracaoLojaData.ListagemEPaginacao = BuscarConfiguracoesDeListagemEPaginacaoDaLoja(identificador);
            configuracaoLojaData.DadosProduto = BuscarDadosDeConfiguracoesDeProdutosDaLoja(identificador);

            return configuracaoLojaData;
        }

        #region Metodos privados

        private List<string> GerarSettings(ICollection<ConfiguracaoLojaCrawler> configuracoes)
        {
            if (configuracoes == null)
                return null;

            List<string> settings = new List<string>();

            foreach (var configuracao in configuracoes)
            {
                if (string.IsNullOrEmpty(configuracao.Settings))
                {
                    ConfiguracaoGenericaCrawler configuracaoGenericaCrawler = configuracaoGenericaCrawlerService.FindById(configuracao.IdentificadorConfiguracaoGenericaCrawler.Value);
                    if (configuracaoGenericaCrawler == null)
                        continue;

                    settings.Add(configuracaoGenericaCrawler.Settings.ResolveBackSlash());
                }
                else
                {
                    settings.Add(configuracao.Settings.ResolveBackSlash());
                }
            }

            return settings;
        }


        private List<ConfiguracaoDadosProdutoData> BuscarDadosDeConfiguracoesDeProdutosDaLoja(long identificadorLoja)
        {
            List<ConfiguracaoDadosProdutoData> configuracoes = new List<ConfiguracaoDadosProdutoData>();

            List<ConfiguracaoDadosProduto> configuracoesDadosProduto =
                configuracaoDadosProdutoService.FindBySpecification(new ConfiguracaoDadosProdutoPorLojaSpecification(identificadorLoja)).ToList();

            if (configuracoesDadosProduto == null && configuracoesDadosProduto.Count <= 0)
                return null;

            foreach (var configuracaoDadosProduto in configuracoesDadosProduto)
            {
                ConfiguracaoDadosProdutoData configuracao = new ConfiguracaoDadosProdutoData();

                configuracao.Nome = configuracaoDadosProduto.Nome;
                configuracao.Campos = BuscarCamposDaConfiguracaoDeProdutoDaLoja(configuracaoDadosProduto.Identificador);
                configuracao.Caracteristicas = BuscarConfiguracaoDaCaracteristicaDaLoja(configuracaoDadosProduto.Identificador);
                configuracao.Imagens = BuscarConfiguracaoDeImagensDaLoja(configuracaoDadosProduto.Identificador);

                configuracoes.Add(configuracao);
            }
            

            return configuracoes;
        }

        private ConfiguracaoImagemData BuscarConfiguracaoDeImagensDaLoja(long identificadorConfiguracaoDadosProduto)
        {
            ConfiguracaoImagem configuracaoImagem
                = configuracaoImagemService.FindSingleBySpecification(new ConfiguracaoImagemPorConfiguracaoDadosProdutoSpecification(identificadorConfiguracaoDadosProduto), 
                                                                      configuracaoImagemService.ConfiguracaoImagemIncludesFilhasETratamentos());

            if (configuracaoImagem == null)
                return null;

            return configuracaoImagem.Adapt<ConfiguracaoImagemData>();
        }

        private ConfiguracaoCaracteristicaData BuscarConfiguracaoDaCaracteristicaDaLoja(long identificadorConfiguracaoDadosProduto)
        {
            ConfiguracaoCaracteristica configuracaoCaracteristica
                = configuracaoCaracteristicaService.FindSingleBySpecification(new ConfiguracaoCaracteristicaPorConfiguracaoDadosProdutoSpecification(identificadorConfiguracaoDadosProduto),
                                                                              configuracaoCaracteristicaService.ConfiguracaoCaracteristicaIncludesFilhos());

            if (configuracaoCaracteristica == null)
                return null;

            return configuracaoCaracteristica.Adapt<ConfiguracaoCaracteristicaData>();
        }

        private List<ConfiguracaoProdutoData> BuscarCamposDaConfiguracaoDeProdutoDaLoja(long identificadorConfiguracaoDadosProduto)
        {
            List<ConfiguracaoProdutoData> configuracoes = new List<ConfiguracaoProdutoData>();
            IEnumerable<ConfiguracaoProduto> configuracoesProduto
                = configuracaoProdutoService.FindBySpecification(new ConfiguracaoProdutoPorConfiguracaoDadosProdutoSpecification(identificadorConfiguracaoDadosProduto), 
                                                                 configuracaoProdutoService.ConfiguracaoProdutoIncludesFilhosETratamentos());

            if (configuracoesProduto == null || configuracoesProduto.ToList().Count <= 0)
                return null;

            List<ConfiguracaoProdutoData> configuracoesProdutoData = configuracoesProduto.Adapt<List<ConfiguracaoProdutoData>>();

            foreach (var c in configuracoesProdutoData)
            {
                c.NextSelector = c.NextSelector.ResolveBackSlash();
                c.TextSelector = c.TextSelector.ResolveBackSlash();
                ConfiguracaoProduto configuracaoProduto = configuracoesProduto.FirstOrDefault(cp => cp.Identificador == c.Identificador);
                c.Nome = configuracaoProduto.NomeCampo;

                if (c.Tratamentos != null)
                {
                    foreach (var tratamento in c.Tratamentos)
                    {
                        tratamento.Regex = !string.IsNullOrEmpty(tratamento.Regex) ? tratamento.Regex.ResolveBackSlash() : tratamento.Regex;
                        tratamento.ValorProcurado = !string.IsNullOrEmpty(tratamento.ValorProcurado) ? tratamento.ValorProcurado.ResolveBackSlash() : tratamento.ValorProcurado;
                        tratamento.ValorSubstituto = !string.IsNullOrEmpty(tratamento.ValorSubstituto) ? tratamento.ValorSubstituto.ResolveBackSlash() : tratamento.ValorSubstituto;
                    }
                }
            }

            return configuracoesProdutoData;
        }

        private ConfiguracaoListagemPaginacaoProdutoData BuscarConfiguracoesDeListagemEPaginacaoDaLoja(long identificadorLoja)
        {
            ConfiguracaoListagemPaginacaoProdutoData configuracao = new ConfiguracaoListagemPaginacaoProdutoData();

            configuracao.Splash = BuscarConfiguracaoSplashListagemEPaginacaoDaLoja(identificadorLoja);
            configuracao.Listagem = BuscarConfiguracaoDeListagemDaLoja(identificadorLoja);
            configuracao.Paginacao = BuscarConfiguracaoDePaginacaoDaLoja(identificadorLoja);            

            return configuracao;
        }

        private ConfiguracaoSplashData BuscarConfiguracaoSplashListagemEPaginacaoDaLoja(long identificadorLoja)
        {
            ConfiguracaoSplash configuracaoSplash =
                configuracaoSplashService.FindSingleBySpecification(new ConfiguracaoSplashPorLojaSpecification(identificadorLoja));

            if (configuracaoSplash == null)
                return null;

            ConfiguracaoSplashData configuracaoSplashData = new ConfiguracaoSplashData();
            configuracaoSplashData.Wait = configuracaoSplash.TempoListagem.HasValue ? configuracaoSplash.TempoListagem.Value : 0;

            return configuracaoSplashData;
        }

        private ConfiguracaoPaginacaoProdutoData BuscarConfiguracaoDePaginacaoDaLoja(long identificadorLoja)
        {
            ConfiguracaoPaginacao configuracaoPaginacao
                = configuracaoPaginacaoService.FindSingleBySpecification(new ConfiguracaoPaginacaoPorIdentificadorLojaSpecification(identificadorLoja));

            if (configuracaoPaginacao == null)
                return null;

            configuracaoPaginacao.NextSelector = configuracaoPaginacao.NextSelector.ResolveBackSlash();

            return configuracaoPaginacao.Adapt<ConfiguracaoPaginacaoProdutoData>();
        }

        private ConfiguracaoListagemProdutoData BuscarConfiguracaoDeListagemDaLoja(long identificadorLoja)
        {
            ConfiguracaoListagem configuracaoListagem
                = configuracaoListagemService.FindSingleBySpecification(new ConfiguracaoListagemPorIdentificadorLojaSpecification(identificadorLoja), 
                                                                        configuracaoListagemService.ConfiguracaoListagemIncludesFilho());

            if (configuracaoListagem == null)
                return null;

            configuracaoListagem.NextSelector = configuracaoListagem.NextSelector.ResolveBackSlash();

            return configuracaoListagem.Adapt<ConfiguracaoListagemProdutoData>();
        }

        private ConfiguracaoCategoriaData BuscarConfiguracaoDaCategoriaDaLoja(long identificadorLoja)
        {
            ConfiguracaoCategoria configuracaoCategoria
                = configuracaoCategoriaService.FindSingleBySpecification(new ConfiguracaoCategoriaPorIdentificadorLojaSpecification(identificadorLoja), 
                                                                         configuracaoCategoriaService.ConfiguracaoCategoriaIncludesFilha());

            if (configuracaoCategoria == null)
                return null;

            configuracaoCategoria.NextSelector = configuracaoCategoria.NextSelector.ResolveBackSlash();
            configuracaoCategoria.TextSelector = configuracaoCategoria.TextSelector.ResolveBackSlash();            

            return configuracaoCategoria.Adapt<ConfiguracaoCategoriaData>();
        }

        private Expression<Func<LojaCrawler, object>>[] IncludesConfiguracoes()
        {
            return new Expression<Func<LojaCrawler, object>>[] { p => p.Configuracoes };
        }
        #endregion
    }
}
