﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoPaginacaoService : DomainServiceRelationalBase<ConfiguracaoPaginacao>
    {
        public ConfiguracaoPaginacaoService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }
    }
}
