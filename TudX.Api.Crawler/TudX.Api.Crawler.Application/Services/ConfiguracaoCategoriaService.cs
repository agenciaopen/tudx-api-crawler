﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoCategoriaService : DomainServiceRelationalBase<ConfiguracaoCategoria>
    {
        public ConfiguracaoCategoriaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        #region Includes
        public Expression<Func<ConfiguracaoCategoria, object>>[] ConfiguracaoCategoriaIncludesFilha()
        {
            return new Expression<Func<ConfiguracaoCategoria, object>>[] { p => p.Filhos };
        }
        #endregion
    }
}
