﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TudX.Api.Crawler.Application.Specification;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;

namespace TudX.Api.Crawler.Application.Services
{
    public class CaracteristicaCrawlerService : DomainServiceRelationalBase<CaracteristicaCrawler>
    {
        public CaracteristicaCrawlerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public List<CaracteristicaCrawler> BuscarCaracteristicasCrawlerDoProduto(long identificadorProduto)
        {
            return this.FindBySpecification(new CaracteristicaCrawlerPorProdutoSpecification(identificadorProduto)).ToList();
        }
    }
}
