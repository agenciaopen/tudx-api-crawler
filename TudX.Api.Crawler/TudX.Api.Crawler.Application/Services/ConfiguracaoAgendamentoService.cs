﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoAgendamentoService : DomainServiceRelationalBase<ConfiguracaoAgendamento>
    {
        public ConfiguracaoAgendamentoService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }
    }
}
