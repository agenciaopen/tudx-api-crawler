﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;
using TudX.Exceptions;

namespace TudX.Api.Crawler.Application.Services
{
    public class LogErroCrawlerService : DomainServiceRelationalBase<LogErroCrawler>
    {
        public LogErroCrawlerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {

        }

        /// <summary>
        /// Salva o log de erro do crawler
        /// </summary>
        /// <param name="logErroCrawler">Entidade de log</param>
        public LogErroCrawler Salvar(LogErroCrawler logErroCrawler)
        {
            this.Save(logErroCrawler);
            this.SaveChanges();

            return logErroCrawler;
        }

        /// <summary>
        /// Busca o log pelo o identificador
        /// </summary>
        /// <param name="identificador">Identificador do log</param>
        /// <returns>Log encontrado.</returns>
        public LogErroCrawler BuscarLogErroCrawlerPorIdentificador(long identificador)
        {
            var logErroCrawler = this.FindById(identificador);

            if (logErroCrawler == null)
                throw new BusinessException("Não foi localizado o log erro do crawler para o identificador informado.");

            return logErroCrawler;
        }
    }
}
