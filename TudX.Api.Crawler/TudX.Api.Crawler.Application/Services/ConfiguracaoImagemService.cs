﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoImagemService : DomainServiceRelationalBase<ConfiguracaoImagem>
    {
        public ConfiguracaoImagemService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        #region Includes
        public Expression<Func<ConfiguracaoImagem, object>>[] ConfiguracaoImagemIncludesFilhasETratamentos()
        {
            return new Expression<Func<ConfiguracaoImagem, object>>[] { p => p.Filhos, p => p.Tratamentos };
        }
        #endregion
    }
}
