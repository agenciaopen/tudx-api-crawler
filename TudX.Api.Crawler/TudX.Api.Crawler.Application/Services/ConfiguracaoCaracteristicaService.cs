﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Crawler.Application.Services
{
    public class ConfiguracaoCaracteristicaService : DomainServiceRelationalBase<ConfiguracaoCaracteristica>
    {
        public ConfiguracaoCaracteristicaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        #region Includes
        public Expression<Func<ConfiguracaoCaracteristica, object>>[] ConfiguracaoCaracteristicaIncludesFilhos()
        {
            return new Expression<Func<ConfiguracaoCaracteristica, object>>[] { p => p.Filhos };
        }
        #endregion
    }
}
