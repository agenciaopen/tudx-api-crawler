﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Crawler.Application.Specification;
using TudX.Api.Crawler.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;

namespace TudX.Api.Crawler.Application.Services
{
    public class ImagemCrawlerService : DomainServiceRelationalBase<ImagemCrawler>
    {
        public ImagemCrawlerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public List<ImagemCrawler> BuscarImagemCrawlerDoProduto(long identificadorProduto)
        {
            return this.FindBySpecification(new ImagemCrawlerPorProdutoSpecification(identificadorProduto)).ToList();
        }

        public List<ImagemCrawler> BuscarImagemCrawlerDosProdutosDaLoja(long identificadorLoja)
        {
            return this.FindBySpecification(new ImagemCrawlerPorLojaSpecification(identificadorLoja), IncludeProduto()).ToList();
        }

        private Expression<Func<ImagemCrawler, object>>[] IncludeProduto()
        {
            return new Expression<Func<ImagemCrawler, object>>[] { i => i.Produto };
        }
    }
}
