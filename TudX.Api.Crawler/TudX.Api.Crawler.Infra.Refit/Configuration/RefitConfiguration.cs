﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Crawler.Infra.Refit.Configuration
{
    public class RefitConfiguration
    {
        public string UrlBaseApiCrawler { get; set; }
        public string UrlBaseApiCrawlerPython { get; set; }
        public string Project { get; set; }
        public string Spider { get; set; }
    }
}
