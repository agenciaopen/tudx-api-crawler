﻿using Microsoft.AspNetCore.Mvc;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TudX.Api.Crawler.Infra.Refit.Clients
{
    public interface IApiCrawler
    {        
        [Get("/healthz")]
        Task<string> HealthChecksAsync();
    }
}
