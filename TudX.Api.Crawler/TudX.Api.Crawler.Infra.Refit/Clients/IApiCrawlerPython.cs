﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TudX.Api.Crawler.CrossCuting.Data.Crawler;
using TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao;

namespace TudX.Api.Crawler.Infra.Refit.Clients
{
    public interface IApiCrawlerPython
    {
        [Multipart]
        [Post("/schedule.json?project={project}&spider={spider}")]
        Task<RetornoCrawler> IniciarCrawler(string project, string spider, [AliasAs("config_loja")] ConfiguracaoLojaData configuracaoLoja, [AliasAs("settings")] List<string> settings);
    }
}
