﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class LojaCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string UrlBase { get; set; }
        public DateTime? DataUltimoProduto { get; set; }
        public ICollection<ConfiguracaoLojaCrawler> Configuracoes { get; set; }

        public LojaCrawler()
        {

        }
    }
}
