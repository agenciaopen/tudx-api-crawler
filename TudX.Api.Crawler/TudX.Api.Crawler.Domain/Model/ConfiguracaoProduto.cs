﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoProduto : IEntity
    {
        public long Identificador { get; set; }
        public string NomeCampo { get; set; }
        public string NextSelector { get; set; }
        public string TextSelector { get; set; }
        public bool EhColecao { get; set; }
        public long? IdentificadorConfiguracaoProdutoSuperior { get; set; }
        public ConfiguracaoProduto ConfiguracaoProdutoSuperior { get; set; }
        public ConfiguracaoDadosProduto ConfiguracaoDadosProduto { get; set; }
        public List<ConfiguracaoProduto> Filhos { get; set; }
        public List<TratamentoProduto> Tratamentos { get; set; }

        public ConfiguracaoProduto()
        {

        }
    }
}
