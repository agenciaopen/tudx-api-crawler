﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class LogErroCrawler : IEntity
    {
        public long Identificador { get; set; }
        public long IdentificadorLoja { get; set; }
        public string Spider { get; set; }
        public string HttpError { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public DateTime Data { get; set; }

        public LogErroCrawler()
        {
            Data = DateTime.Now;
        }
    }
}
