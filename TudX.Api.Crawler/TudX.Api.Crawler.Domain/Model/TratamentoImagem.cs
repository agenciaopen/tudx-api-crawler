﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model.Enum;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class TratamentoImagem : IEntity
    {
        public long Identificador { get; set; }
        public TipoTratamento Tipo { get; set; }
        public string ValorProcurado { get; set; }
        public string ValorSubstituto { get; set; }
        public ConfiguracaoImagem ConfiguracaoImagem { get; set; }

        public TratamentoImagem()
        {

        }
    }
}
