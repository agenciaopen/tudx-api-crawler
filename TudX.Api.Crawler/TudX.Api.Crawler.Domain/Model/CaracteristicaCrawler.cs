﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class CaracteristicaCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
        public ProdutoCrawler Produto { get; set; }

        public CaracteristicaCrawler(string nome, string valor)
        {
            Nome = nome;
            Valor = valor;
        }

        public CaracteristicaCrawler()
        {

        }
    }
}
