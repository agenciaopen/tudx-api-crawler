﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model.Enum;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoAgendamento : IEntity
    {
        public long Identificador { get; set; }
        public long IdentificadorLoja { get; set; }
        public TipoFrequencia TipoFrequencia { get; set; }
        public int? ValorFrequencia { get; set; }
        public TimeSpan? HoraFrequencia { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
    }
}
