﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ProdutoCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string NomeCategoria { get; set; }
        public string CodigoProduto { get; set; }
        public string Descricao { get; set; }
        public string UrlAnuncio { get; set; }
        public decimal Valor { get; set; }
        public string FormaPagamento { get; set; }
        public long IdentificadorLoja { get; set; }
        public ICollection<CaracteristicaCrawler> Caracteristicas { get; set; }
        public ICollection<ImagemCrawler> Imagens { get; set; }

        public ProdutoCrawler(string nome, string nomeCategoria, string descricao, string urlAnuncio, decimal valor, string formaPagamento)
        {
            Nome = nome;
            NomeCategoria = nomeCategoria;
            Descricao = descricao;
            UrlAnuncio = urlAnuncio;
            Valor = valor;
            FormaPagamento = formaPagamento;
        }

        public ProdutoCrawler()
        {

        }
    }
}
