﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoCategoria : IEntity
    {
        public long Identificador { get; set; }
        public string UrlBase { get; set; }
        public string UrlSelector { get; set; }
        public string TextSelector { get; set; }
        public string NextSelector { get; set; }
        public string Dominio { get; set; }
        public long? IdentificadorConfiguracaoCategoriaSuperior { get; set; }
        public ConfiguracaoCategoria ConfiguracaoCategoriaSuperior { get; set; }
        public LojaCrawler Loja { get; set; }
        public List<ConfiguracaoCategoria> Filhos { get; set; }        

        public ConfiguracaoCategoria()
        {

        }
    }
}
