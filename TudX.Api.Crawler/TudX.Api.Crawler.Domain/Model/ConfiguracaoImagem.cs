﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoImagem : IEntity
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string UrlSelector { get; set; }
        public string Dominio { get; set; }
        public long? IdentificadorConfiguracaoImagemSuperior { get; set; }
        public ConfiguracaoImagem ConfiguracaoImagemSuperior { get; set; }
        public ConfiguracaoDadosProduto ConfiguracaoDadosProduto { get; set; }
        public List<ConfiguracaoImagem> Filhos { get; set; }
        public List<TratamentoImagem> Tratamentos { get; set; }


        public ConfiguracaoImagem()
        {

        }
    }
}
