﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ImagemCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
        public ProdutoCrawler Produto { get; set; }

        public ImagemCrawler(string urlImagem)
        {            
            UrlImagem = urlImagem;
        }

        public ImagemCrawler()
        {

        }
    }
}
