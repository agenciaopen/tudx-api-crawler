﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Crawler.Domain.Model.Enum
{
    public enum TipoTratamento : short
    {
        Nenhum = 0,
        Replace = 1,
        Regex = 2
    }
}
