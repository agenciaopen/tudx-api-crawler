﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Crawler.Domain.Model.Enum
{
    public enum TipoFrequencia : short
    {
        Nenhum = 0,
        Hora = 1,
        Dia = 2        
    }
}
