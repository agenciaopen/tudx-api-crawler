﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Crawler.Domain.Model.Enum
{
    public enum TipoLog : short
    {
        Nenhum = 0,
        Sucesso = 1,
        Erro = 2
    }
}
