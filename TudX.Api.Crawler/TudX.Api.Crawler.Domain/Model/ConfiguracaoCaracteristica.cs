﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoCaracteristica : IEntity
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
        public long? IdentificadorConfiguracaoCaracteristicaSuperior { get; set; }
        public ConfiguracaoDadosProduto ConfiguracaoDadosProduto { get; set; }
        public ConfiguracaoCaracteristica ConfiguracaoCaracteristicaSuperior { get; set; }
        public List<ConfiguracaoCaracteristica> Filhos { get; set; }

        public ConfiguracaoCaracteristica()
        {

        }
    }
}
