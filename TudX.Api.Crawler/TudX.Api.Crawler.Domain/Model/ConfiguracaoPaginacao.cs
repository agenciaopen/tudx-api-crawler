﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoPaginacao : IEntity
    {
        public long Identificador { get; set; }
        public string NextSelector { get; set; }
        public string UrlSelector { get; set; }
        public string Dominio { get; set; }
        public string ParametroGet { get; set; }
        public LojaCrawler Loja { get; set; }

        public ConfiguracaoPaginacao()
        {

        }
    }
}
