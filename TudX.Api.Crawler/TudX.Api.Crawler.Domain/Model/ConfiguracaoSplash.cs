﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoSplash : IEntity
    {
        public long Identificador { get; set; }
        public short? TempoCategoria { get; set; }
        public short? TempoListagem { get; set; }
        public short? TempoProduto { get; set; }
        public long IdentificadorLoja { get; set; }

        public ConfiguracaoSplash()
        {

        }
    }
}
