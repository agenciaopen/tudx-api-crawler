﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoLojaCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string Settings { get; set; }
        public LojaCrawler Loja { get; set; }
        public long? IdentificadorConfiguracaoGenericaCrawler { get; set; }

        public ConfiguracaoLojaCrawler()
        {

        }
    }
}
