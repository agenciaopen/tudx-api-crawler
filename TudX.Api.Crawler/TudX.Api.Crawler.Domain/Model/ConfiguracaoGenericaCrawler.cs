﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoGenericaCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string Settings { get; set; }
        public bool Ativo { get; set; }

        public ConfiguracaoGenericaCrawler()
        {

        }
    }
}
