﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class ConfiguracaoDadosProduto : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long IdentificadorLoja { get; set; }
        public ICollection<ConfiguracaoProduto> ConfiguracoesProduto { get; set; }
        public ICollection<ConfiguracaoCaracteristica> ConfiguracoesCaracteristica { get; set; }
        public ICollection<ConfiguracaoImagem> ConfiguracoesImagem { get; set; }

        public ConfiguracaoDadosProduto()
        {

        }
    }
}
