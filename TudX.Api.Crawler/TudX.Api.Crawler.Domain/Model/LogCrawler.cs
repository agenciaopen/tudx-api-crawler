﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Crawler.Domain.Model.Enum;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class LogCrawler : IEntity
    {
        public long Identificador { get; set; }
        public long IdentificadorLoja { get; set; }
        public DateTime DataInicio { get; set; }
        public string MensagemStatus { get; set; }
        public TipoLog TipoLog { get; set; }

        public LogCrawler()
        {

        }
    }
}
