﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Crawler.Domain.Model
{
    public class NotificacaoProdutoCrawler : IEntity
    {
        public long Identificador { get; set; }
        public string Propriedades { get; set; }
        public DateTime DataIdentificacao { get; set; }
        public bool FoiVerificado { get; set; }
        public string Emails { get; set; }

        public NotificacaoProdutoCrawler()
        {

        }
    }
}
