﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using TudX.Adapter.Eventbus.RabbitMQ.Extensions;
using TudX.Adapter.Quartz.Extensions;
using TudX.Adapter.Quartz.Model;
using TudX.Api.Crawler.BackgroundTasks.Job;
using TudX.Api.Crawler.Infra.EF.DependencyInjection;
using TudX.Api.Crawler.Infra.Refit.Configuration;
using TudX.Api.Crawler.Infra.Refit.DependencyInjection;
using TudX.Extensions;
using TudX.Http.Client.AspNetCore.DependencyInjection;

namespace TudX.Api.Crawler.BackgroundTasks
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        /// <summary>
        /// Construtor.
        /// </summary>
        /// <param name="configuration">Informações de configurações da aplicação.</param>
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAspNetCoreHttpClient();
            services.AddRefitAdapter(configuration.SafeGet<RefitConfiguration>());
            services.AddQuartz();
            services.AddEFAdapter(configuration);
            services.AddRabbitMQAdapter(configuration);

            services.AddSingleton<HealthChecksJob>();
            services.AddSingleton(new JobSchedule(
                jobType: typeof(HealthChecksJob),
                cronExpression: "0/10 * * * * ?")); // run every 10 seconds

            services.AddSingleton<LojaJob>();
            services.AddSingleton(new JobSchedule(
                jobType: typeof(LojaJob),
                cronExpression: "0 */5 * ? * *")); // run every 5 minutes

            services.AddSingleton<EventoEnviadoJob>();
            services.AddSingleton(new JobSchedule(
                jobType: typeof(EventoEnviadoJob),
                cronExpression: "0 */2 * ? * *")); // run every 2 minutes
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }
}
