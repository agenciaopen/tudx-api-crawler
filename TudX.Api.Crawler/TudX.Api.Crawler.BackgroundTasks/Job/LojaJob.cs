﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Crawler.Application.Services;
using TudX.Api.Crawler.Application.Specification;
using TudX.Api.Crawler.Application.Util;
using TudX.Api.Crawler.CrossCuting.Data.Crawler;
using TudX.Api.Crawler.CrossCuting.Data.Loja.Configuracao;
using TudX.Api.Crawler.Domain.Model;
using TudX.Api.Crawler.Domain.Model.Enum;
using TudX.Api.Crawler.Infra.Refit.Clients;
using TudX.Api.Crawler.Infra.Refit.Configuration;

namespace TudX.Api.Crawler.BackgroundTasks.Job
{
    public class LojaJob : IJob
    {
        private readonly ILogger<LojaJob> _logger;
        private readonly IServiceProvider serviceProvider;
        private readonly RefitConfiguration refitConfiguration;

        public LojaJob(IServiceProvider serviceProvider, ILogger<LojaJob> logger, RefitConfiguration refitConfiguration)
        {
            _logger = logger;
            this.serviceProvider = serviceProvider;
            this.refitConfiguration = refitConfiguration;
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                _logger.LogInformation("Iniciando...");
                using (var scope = serviceProvider.CreateScope())
                {
                    LojaCrawlerService lojaCrawlerService = new LojaCrawlerService(scope.ServiceProvider);
                    LogCrawlerService logCrawlerService = new LogCrawlerService(scope.ServiceProvider);
                    List<LojaCrawler> lojas = lojaCrawlerService.BuscarTodasAsLojas();
                    List<LojaCrawler> lojasParaCrawler = BuscarLojasParaIniciarCrawler(scope, lojas);
                    var apiCrawlerPython = scope.ServiceProvider.GetService<IApiCrawlerPython>();

                    foreach (var lojaCrawler in lojasParaCrawler)
                    {
                        ConfiguracaoLojaData configuracaoLojaData = lojaCrawlerService.BuscarConfiguracaoDaLoja(lojaCrawler.Identificador);
                        try
                        {
                            RetornoCrawler retornoCrawler = apiCrawlerPython.IniciarCrawler(this.refitConfiguration.Project, this.refitConfiguration.Spider, configuracaoLojaData, configuracaoLojaData.Settings).Result;
                            logCrawlerService.RegistrarInicioDoCrawler(lojaCrawler.Identificador, retornoCrawler.status);
                        }
                        catch (Exception ex)
                        {
                            logCrawlerService.RegistrarInicioDoCrawler(lojaCrawler.Identificador, ex.Message, TipoLog.Erro);
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return Task.CompletedTask;
        }

        private List<LojaCrawler> BuscarLojasParaIniciarCrawler(IServiceScope scope, List<LojaCrawler> lojas)
        {
            ConfiguracaoAgendamentoService configuracaoAgendamentoService = new ConfiguracaoAgendamentoService(scope.ServiceProvider);
            List<LojaCrawler> lojasCandidatas = new List<LojaCrawler>();

            foreach (var lojaCandidata in lojas)
            {
                ConfiguracaoAgendamento configuracaoAgendamento =
                    configuracaoAgendamentoService.FindSingleBySpecification(new ConfiguracaoAgendamentoPorLojaSpecification(lojaCandidata.Identificador));

                if (configuracaoAgendamento == null || configuracaoAgendamento.DataFim.HasValue)
                    continue;

                if (FrequenciaHelper.ValidaSeAFrequenciaFoiAtingida
                    (configuracaoAgendamento.TipoFrequencia, configuracaoAgendamento.ValorFrequencia, configuracaoAgendamento.HoraFrequencia, lojaCandidata.DataUltimoProduto.Value))
                    lojasCandidatas.Add(lojaCandidata);
            }

            return lojasCandidatas;
        }
    }
}
