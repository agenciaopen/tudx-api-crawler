﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.IntegrationEvents.Application.Model;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Crawler.BackgroundTasks.Job
{
    public class EventoEnviadoJob : IJob
    {
        private readonly ILogger<EventoEnviadoJob> _logger;
        private readonly IServiceProvider serviceProvider;
        private readonly IEventBus eventBus;

        public EventoEnviadoJob(IServiceProvider serviceProvider, ILogger<EventoEnviadoJob> logger, IEventBus eventBus)
        {
            _logger = logger;
            this.serviceProvider = serviceProvider;
            this.eventBus = eventBus;
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    EventoEnviadoService eventoEnviadoService = new EventoEnviadoService(scope.ServiceProvider);
                    List<EventoEnviado> eventosParaEnviar = eventoEnviadoService.BuscarEventosEnviadosQueEstaoProntosParaOReenvio(DateTime.UtcNow, 5);

                    if (eventosParaEnviar != null && eventosParaEnviar.Count > 0)
                    {
                        foreach (var eventoEnviado in eventosParaEnviar)
                        {
                            eventBus.Publish(eventoEnviado);
                            eventoEnviado.QuantidadeEnvio++;
                            eventoEnviadoService.Save(eventoEnviado);                            
                        }

                        eventoEnviadoService.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
            }

            return Task.CompletedTask;
        }
    }
}
